		var taxaMap = {};
        var geneMap = {};
        
        function selectTaxa(taxaName)
	    {
	        	taxa = '.' + taxaName;
				if (taxaMap[taxaName]  == undefined){
					taxaMap[taxaName] = 0;
				}
	        	
	        	if (taxaMap[taxaName] == 0){
	        		taxaMap[taxaName] = 1;
	        		
	        	}else if(taxaMap[taxaName] == 1){
	        		taxaMap[taxaName] = 0;
	        		
	        	}
	        	
	        	
	    }
		function getAndSelectTaxa(){
			
			selectTaxa($('#selTaxa').val());
			setTaxaSelectedList();
			correctSelections();
		}
		function getAndSelectGene(){
			selectGene($('#selGene').val());
			setGeneSelectedList();
			correctSelections();
		}
		
		function setTaxaSelectedList(){
        	$('#selTaxa').empty();
			for(x in taxaMap){
				if(taxaMap[x] == 1){
					$('#selTaxa').append('<option value="'+x+'">'+ x + '</option>');
				}
			}
        }
        
	    function selectGene(geneName)
	    	{
	        	gene = "." + geneName;
	        	if (geneMap[geneName]  == undefined){
					geneMap[geneName] = 0;
				}
	        	if (geneMap[geneName] == 0){
	        		geneMap[geneName] = 1;
	        	}else if(geneMap[geneName] == 1){
	        		geneMap[geneName] = 0;
	        	}
	        	
	    }
	    function setGeneSelectedList(){
	    	$('#selGene').empty();
        	for(x in geneMap){
				if(geneMap[x] == 1){
					$('#selGene').append('<option value="'+x+'">'+ x + ' </option>');
				}
        	}
	    }



        $("#colorMap td[title]").tooltip();
        $("#globalMatrix td[title]").tooltip();
        var taxaMap = {};
        var geneMap = {};
        function colorMapSelect(taxaName, geneName){
			selectTaxa(taxaName);
			selectGene(geneName);
        }
        function comboSelectTaxa(){
        	var sel = $('#tCombo').val();
        	selectTaxa(sel);
        	correctSelections();
        }

        function comboSelectGene(){
        	var sel = $('#gCombo').val();
        	selectGene(sel);
        	correctSelections();
        }
	    function sendMail(urlSend)
    	{
        	var taxaArr = new Array();
        	var geneArr = new Array();
	    	
    		for(x in taxaMap){
				if(taxaMap[x] == 1){
					taxaArr.push(x);
				}
        	} 
    		for(x in geneMap){
				if(geneMap[x] == 1){
					geneArr.push(x);
				}
        	}
        	var geneStr = geneArr.join();
        	var taxaStr = taxaArr.join();
			var outputType = $('#outputType').val();
			 
        	$.ajax({
				url: urlSend, 
				data: {taxa: taxaStr, genes: geneStr, outputType: outputType},
				async: true,
				success: function(msg){
						alert('An email with your sequence file(s) will be sent to you shortly.');
					},
				error: function(msg){
						alert('There was an error processing your request.');
					}
	        	});
    	}

	    function dlGenome(urlSend)
    	{
	    	var taxaList = new Array();
	    	$.each($('.taxaSelect:checked'),function(index,value){
	    		taxaList.push(value.name);
	    	});

        	var taxaStr = taxaList.join(',');
			var outputType = "fasta";
			var genome = true;
        	$.ajax({
				url: urlSend, 
				data: {taxa: taxaStr, outputType: outputType, genome: genome},
				async: true,
				success: function(msg){
						alert('An email with your sequence file(s) will be sent to you shortly.');
					},
				error: function(msg){
						alert('There was an error processing your request.');
					}
	        	});
    	}
	    
		function taxaGeneSelectMenu(btlURL,bglURL,detailedMapUrl){

			$('#res').empty();
			$('#res').append('<div  class="span-7"  id="taxaList" >');
			$('#taxaList').append('<p>loading data.  please wait</p>');
			$('#res').append('</div>');
			$('#res').append('<div class="span-2">&nbsp');
			$('#res').append('</div>');
			$('#res').append('<div  class="span-7" id="geneList" >');
			$('#res').append('</div>');
			
			$.ajax({ 
				url: btlURL,
				async: true,
				success: function( htmlData ){
					$('#taxaList').empty();
					$('#taxaList').append('<div class="span-7 last"><h5>Please select the taxa you wish to retrieve from the database.</h5></div>');
					$('#taxaList').append('<div class="span-7 last">');
					$('#taxaList').append(htmlData);
					$('#taxaList').append('</div>');
					$('#taxaList').append('<div class="span-17 last" style="text-align:center;"><a onclick="submitToMap(\''+detailedMapUrl+'\')" style="text-decoration:none;">Draw detailed map with the selected taxa and genes.</a></div>');
					setList();
				},
				error: function( msg ){ alert('There was an error getting the taxa list.');}
			});
			$.ajax({ 
				url: bglURL,
				async: true,
				success: function( htmlData ){
					$('#geneList').empty();
					$('#geneList').append('<div class="span-7 last"><h5>Please select the genes you wish to retrieve. Hold ctrl for multiple selections or command on the macintosh platform.</h5></div>');
					$('#geneList').append('<div class="span-7 last" style="text-align:center;">');
					$('#geneList').append(htmlData);
					$('#geneList').append('</div>');
				},
				error: function( msg ){ alert('There was an error getting the taxa list.');}
			});
			
			
		}
		function setList(){
			$('ul#test').collapsibleCheckboxTree({
				checkParents : false, // When checking a box, all parents are checked
				checkChildren : true, // When checking a box, all children are checked
				uncheckChildren : true, // When unchecking a box, all children are unchecked
				initialState : 'collapse' // Options - 'expand' (fully expanded), 'collapse' (fully collapsed) or default
			});
		}

		function submitToMap(detailedMapURL){
			var taxaList = $('.taxaSelect:checked');
			var geneList = $('.geneSelect:selected');
			var taxaRet = new Array();
			var geneRet = new Array();
			var geneNames = new Array();
			for(var i = 0; i < taxaList.length; i++){
				taxaRet.push(taxaList[i].name);
			}
			for(var i = 0; i < geneList.length; i++){
				geneRet.push(geneList[i].value);
				geneNames.push(geneList[i].text);
			}
			
			$.ajax({
				url: detailedMapURL, 
				data: {taxa: taxaRet.join(','), genes: geneRet.join(',')},
				async: true,
				beforeSend:function(jqXHR,settings){
					$('div#Specific_Matrix').empty();
					$('div#Specific_Matrix').append("<div style='text-align:center'><img src='/AlignDB/images/spinner.gif' /></div>");
				},
				success: function(htmlData){
						var spans = '';
						$('div#Specific_Matrix').empty();
						$('div#Specific_Matrix').append(spans);
						$.each(taxaRet, function(index, value) {
							  selectTaxa(value); 
						});
						$.each(geneNames, function(index, value) { 
							  selectGene(value); 
						});
						$('div#Specific_Matrix').append(htmlData);
						setTaxaSelectedList();
						setGeneSelectedList();
						correctSelections();
					},
				error: function(msg){
						alert('Error sending detailedMap list to the server');
					}
	        });
			
		}

		

    	function drawColorMap(colorMapURL)
    	{	
	    	$('#res').empty();
	    	$('#res').append(
			'<p>loading data.  please wait</p>'
			)
			$.ajax({ 
				url: colorMapURL,
				async: true,
				success: function( htmlData ){
					$('#res').empty();
					$('#res').append(htmlData);
				},
				error: function( msg ){ alert('There was an error getting the color map. ');}
			});
    	}
    	
    	function hide( idName ){       
     		$('.showHide').addClass('hidden');
 		}
