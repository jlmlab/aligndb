package edu.uga.aligndb

import java.util.Date;
import java.io.File;
import java.io.OutputStream;
import org.biojavax.bio.seq.RichSequence.IOTools
import org.biojava.bio.seq.DNATools;
import edu.uga.aligndb.Sequence;
import org.biojava.bio.seq.db.*
import org.biojava.bio.symbol.AlphabetManager;

import org.codehaus.groovy.grails.commons.*

import sun.rmi.runtime.Log;
import sun.util.logging.resources.logging;

class MuscleService {
	
    static transactional = true
	def nexusService
	def backgroundService
	def config = ConfigurationHolder.config
	
    def doMuscleAlignment(List taxaList, List geneList, Closure callBackFn,boolean asNexus) {
		def files = [];
		try{
			def taxaSeqMap = [:]
			def masterMap = [:]
			def masterCount = 0;
			def tempDir = createTempDir()
			def muscleOutput = createTempDir()
			backgroundService.execute("aligning"){
				for (gene in geneList){
					def fileName = "${gene.name}.fasta"
					File geneFile = new File(tempDir,fileName)
					OutputStream geneIO =  geneFile.newOutputStream()
					
					SequenceDB db = new HashSequenceDB();
					
					for (taxa in taxaList){
						def seq = Sequence.findByTaxaAndGene(taxa,gene)
	
						if (seq != null){
							org.biojava.bio.seq.Sequence dna = DNATools.createDNASequence(seq.sequence, taxa.name)
							db.addSequence(dna)
						}
					}
					IOTools.writeFasta(geneIO, db.sequenceIterator(),	null)
					geneIO.flush()
					def outFile = muscleOutput.getAbsolutePath() + "/${fileName}"
					
					geneFile.setReadable true, false
					def inFile = geneFile.getAbsolutePath()
					String execStr = "${config.muscle.executable.path} -in ${inFile} -out ${outFile} -quiet"
					
					File of = new File(outFile);
					
					//log.error execStr
					Process p = Runtime.getRuntime().exec(execStr)
					p.waitFor()
					close(p.errorStream)
					close(p.errorStream)
					close(p.errorStream)
					p.destroy()
					//execStr.execute()
					 
					
					
					log.debug "Aligning ${gene.name}: Done"
					if(of.exists()){
							files << outFile
							if(asNexus){
								BufferedReader br = new BufferedReader(new FileReader(outFile))
								def a = AlphabetManager.alphabetForName("DNA")
								def seqCollection = IOTools.readFastaDNA( br, null )
								int nchars = 0;
								while(seqCollection.hasNext()){
									def seq = seqCollection.nextSequence()
									taxaSeqMap.put seq.getName()	, 	seq.getStringSequence()
									nchars = seq.getSequenceLength()
								}
								taxaSeqMap.put "seqSize", nchars
							}
					}
					else{
						if(!asNexus)
						log.error "Aligning ${gene.name}: No output file, will not be sent."
						
					}

					if(asNexus){
						StringBuffer seqBuff= new StringBuffer();
						for(int i = 0; i < taxaSeqMap.get("seqSize"); i++){
							seqBuff << 'N'
						}
						for(taxa in taxaList){
							if(!taxaSeqMap.containsKey(taxa.name)){
								taxaSeqMap.put taxa.name, seqBuff.toString()
							}
						}
						masterCount += taxaSeqMap.get("seqSize")
						masterMap.put gene.name, taxaSeqMap
                        //dereference the taxaSeqMap so the next gene data does not overwrite the current one.
                        taxaSeqMap = [:]
					}
				}
				
				if(asNexus){
					masterMap.put "TotalCharacterCount", masterCount
					files = nexusService.createNexusFromMuscleAlignments(taxaList,masterMap)
				}
				
				callBackFn.call(files.collect {new File(it) })
			}
			
			
		}catch(Exception e){
			log.error e.toString()
			e.printStackTrace()
		}
		
		
    }

	public static File createTempDir() {
		final String baseTempPath = System.getProperty("java.io.tmpdir");
	
		Random rand = new Random();
		int randomInt = 1 + rand.nextInt();
	
		File tempDir = new File(baseTempPath + File.separator + "tempDir" + randomInt);
		if (tempDir.exists() == false) {
			tempDir.mkdir();
		}
	
		tempDir.deleteOnExit();
	
		return tempDir;
	}
		
	private static void close(Closeable c) {
		if (c != null) {
		  try {
			c.close();
		  } catch (IOException e) {
			// ignored
		  }
		}
	  }
}
