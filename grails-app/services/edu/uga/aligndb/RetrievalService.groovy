package edu.uga.aligndb

import java.util.List;


class RetrievalService {
	static User[] getUsersWithRole(long roleId) {
		def userRoleList = UserRole.findAll("from UserRole where role.id = ${roleId}")
		def userIds = userRoleList.collect {it.user.id }.flatten()
		def UserList = []
		userIds.each {
				UserList.add User.get(it)
			 }
		return UserList
	}
	static User[] getUsersRoles
	static List get(long taxaId, long geneId) {
		Sequence.findAll("from Sequence where taxa.id=${taxaId} and gene.id=${geneId}")
	}
}