package edu.uga.aligndb

import java.io.BufferedReader;
import java.util.List;

import org.biojava.bio.*;
import org.biojava.bio.seq.*;
import org.biojava.bio.seq.db.*;
import org.biojava.bio.seq.io.*;
import org.biojava.bio.symbol.*;
import org.biojavax.bio.seq.RichSequence.IOTools.*;
import org.biojavax.bio.seq.*;
import org.codehaus.groovy.grails.web.json.*;


class UploadService {

	static transactional = true
	def springSecurityService

	boolean loadGene(String geneName, String fileName, Boolean overWrite){

		try{

			BufferedReader bRead = new BufferedReader(new FileReader(fileName));  

			RichSequenceIterator iter = RichSequence.IOTools.readFastaDNA(bRead, null);


			def gene = Gene.findByName(geneName);
			if(!gene){

				gene = new Gene(name:geneName);
				gene.save(flush: true);
			}
			while(iter.hasNext()){

				SimpleRichSequence seq = iter.nextSequence();
				def taxaName = seq.getName();
				def seqString = seq.seqString();
				def taxa = Taxa.findByName(taxaName);
				if(!taxa){

					taxa = new Taxa(name:taxaName);
					taxa.save(flush: true);
				}

				def seqObj = Sequence.findByTaxaAndGene(taxa, gene);
				def userDetails = springSecurityService.principal
				def primaryUser = User.get(userDetails.id)
				if(!seqObj){
					seqObj = new Sequence(taxa:taxa, gene:gene, sequence: seqString, state:'Complete', theOne:true, primaryUser:primaryUser);
					seqObj.save(flush: true);
				}else if(overWrite){ 
					seqObj.theOne = false;
					seqObj.save(flush: true);
					seqObj = new Sequence(taxa:taxa, gene:gene, sequence: seqString, state:'Complete', theOne:true, primaryUser:primaryUser);
					seqObj.save(flush: true);
				}else {
					log.error("Sequence for taxa: ${taxa} and gene: ${gene} already present in the database")
				}

			}
			return true;
		}catch(Exception ex){
			log.error(ex.getMessage());
			return false;	
		}
		finally{
			bRead.close();
		}
	}
	boolean uploadSingleGene(String taxaName, String geneName, String seqString,boolean overwrite){
		
			try{
				def taxa = Taxa.findByName(taxaName);
				if(!taxa){
					taxa = new Taxa(name:taxaName, family:Family.findById(1));
					taxa.save(flush: true);
				}
				
				def gene = Gene.findByName(geneName);
				if(!gene){
					gene = new Gene(name:geneName);
					gene.save(flush: true);
				}
				
				def userDetails = springSecurityService.principal
				def primaryUser = User.get(userDetails.id)
				
				def seqObj = Sequence.findByTaxaAndGene(taxa, gene);
				if(!seqObj){
					seqObj = new Sequence(taxa:taxa, gene:gene, sequence: seqString, state:'Complete', theOne:true, primaryUser:primaryUser);
					seqObj.save(flush: true);
				}else if(overwrite){
					seqObj.theOne = false;
					seqObj.save(flush: true);
					seqObj = new Sequence(taxa:taxa, gene:gene, sequence: seqString, state:'Complete', theOne:true, primaryUser:primaryUser);
					seqObj.save(flush: true);
				}else {
					log.error("Sequence for taxa: ${taxa} and gene: ${gene} already present in the database")
				}
				return true;
			}catch(Exception ex){
				log.error(ex.getMessage());
				return false;
			}
		}
	boolean loadTaxon(String taxaName, String fileName, Boolean overWrite){
		try{
			
			BufferedReader bRead = new BufferedReader(new FileReader(fileName));  

			RichSequenceIterator iter = RichSequence.IOTools.readFastaDNA(bRead, null);


			def taxa = Taxa.findByName(taxaName);
			if(!taxa){

				taxa = new Taxa(name:taxaName);     
				taxa.save(flush: true);     
			}
			while(iter.hasNext()){
				
				
				SimpleRichSequence seq = iter.nextSequence();
				def geneName = seq.getName();
				def seqString = seq.seqString();
					
				def gene = Gene.findByName(geneName);     
				
				if(!gene){
					
					gene = new Gene(name:geneName); 
					gene.save(flush: true);    
				}
				def userDetails = springSecurityService.principal
				def primaryUser = User.get(userDetails.id)
				def seqObj = Sequence.findByTaxaAndGene(taxa, gene);
				if(!seqObj){
					seqObj = new Sequence(taxa:taxa, gene:gene, sequence: seqString, state:'Complete', theOne:true, primaryUser:primaryUser);
					seqObj.save(flush: true);
				}else if(overWrite){ 
					seqObj.theOne = false;
					seqObj.save(flush: true);
					seqObj = new Sequence(taxa:taxa, gene:gene, sequence: seqString, state:'Complete', theOne:true, primaryUser:primaryUser);
					seqObj.save(flush: true);
				}else {
					log.error("Sequence for taxa: ${taxa} and gene: ${gene} already present in the database")
				}
			}
			return true;
		}catch(Exception ex){
			log.error(ex.getMessage());
			return false;	
		}
		finally{
			bRead.close();
		}
	}
	
	//returns JSONObject of JSONObjects each containing a gene and sequence	
	def uploadTaxon(String taxaName, String fileName){
		BufferedReader bRead;
		RichSequenceIterator iter;
		def seqList = [:]
		seqList['taxaName'] = taxaName
		
		try{
			
			bRead = new BufferedReader(new FileReader(fileName));

			iter = RichSequence.IOTools.readFastaDNA(bRead, null);
			
			int i = 0;
			int index = 0;
			def geneList = [];
			Gene.listOrderByName().each{geneList.add(index, it.toString()); index++;}   //retrieves gene list from DB
			
			while(iter.hasNext()){
				SimpleRichSequence seq = iter.nextSequence();
				
				def map = ['geneName':seq.getName(), 'seqString':seq.seqString()]
				
				//uses helper function rather than DB to query the gene domain
				if(!(geneCheck(geneList, map['geneName']))){  map['ambiguous'] = true;}  //checks if gene exists in DB
				else {map['ambiguous'] = false}
				
				def flag = false
				seqList.each{ key,value ->
					if(key != 'taxaName'){
						
						if(value['geneName']== seq.getName()){
							value['duplicate']=true
							flag = true
						}	
					}
				}
				map['duplicate']=flag;
				
				seqList["map" + i] = map;	
				i++;
			}
		}
		catch(Exception ex){
			log.errAor(ex.getMessage());
			return [:];
		}
		finally{
			bRead.close();
		}
		return seqList;
	}
	
	boolean saveTaxon(Map seqList){
		
		seqList.each{key, value -> 
			if(key != 'taxaName'){
				if(key['ambiguous'] == false && key['duplicates'] == false){
					
				}
			}
		}
	}
	
	boolean geneCheck(geneList, gene){
		def exists = false;
		geneList.each{ 
			if(it == gene)
				exists = true;
		}
		return exists
	}
	
	def uploadGenome(String taxaName, String fileName){
		BufferedReader bRead
		String genomeSeq = ""
		def genomeObj = [:]
	
		genomeObj['taxaName'] = taxaName
		CharSequence carrot
		
		
		try{
			
			bRead = new BufferedReader(new FileReader(fileName))
			
			carrot = '>'
			
			while (bRead.readLine() != null){
				if(bRead.readLine().contains(carrot) == false){
					genomeSeq += bRead.readLine()	
				}
			}
			
			genomeObj['sequence'] = genomeSeq
			
			def taxa = Taxa.findByName(taxaName);
			if(!taxa){
				taxa = new Taxa(name:taxaName);
				taxa.save(flush: true);
			}
			
			def userDetails = springSecurityService.principal
			def primaryUser = User.get(userDetails.id)
			
			def genome = Genome.findByTaxa(taxa);
			
			if(!genome){
				genome = new Genome(taxa:taxa, sequence:genomeSeq, state:"Complete", theOne:true, primaryUser:primaryUser);
				genome.save(flush:true);
			}
			else{
				genome.theOne = false;
				genome.save(flush: true);
				genome = new Genome(taxa:taxa, sequence:genomeSeq, state:'Complete', theOne:true, primaryUser:primaryUser);
				genome.save(flush: true);
			}
			
			return genomeObj
			
		}catch(Exception ex){
			log.error(ex.getMessage());	
		
		}
		finally{
			bRead.close();
		}
	}
}