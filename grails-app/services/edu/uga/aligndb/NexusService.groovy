package edu.uga.aligndb

class NexusService {
	
    static transactional = true
	
    def createNexusFromMuscleAlignments(List taxaList, LinkedHashMap ml ) {
		
		Random rand = new Random();
		int randomInt = 1 + rand.nextInt();
		
		def nexusOutput 	=	createTempDir();
		def nexusOutFile 	= 	nexusOutput.getAbsolutePath() + "/MonAToL_${randomInt}.nex"
		
		
		File onf = new File(nexusOutFile)
		onf.createNewFile()
		
		if(onf.exists()){
			onf.withWriter {out->
				StringBuffer buff = new StringBuffer()
				buff << "#nexus\n"
				buff << "[This file was automatically generated by the MonATol Database application]\n"
				buff << "[To report any problems with the nexus file formatting please contact Raj Ayyampalayam: raj76@uga.edu]\n"
				
				
				//code for the data block
				def totalCharCount = ml.get("TotalCharacterCount");
				buff << "begin data;\n"
				buff << "\tdimensions ntax=${taxaList.size()} nchar=${totalCharCount};\n"
				buff << "\tformat datatype=dna missing=N gap=-;\n"
				buff << "\tmatrix\n"
				
				int lastEnding = 0;
				def geneIndices = [:]
				ml.each {gene,map ->
					if(gene!="TotalCharacterCount"){
						buff  << "\n\n\t\t[generated by the muscle alignment on gene: ${gene}]\n"
						printSixtyAtATime(buff,map,taxaList)
						lastEnding += map.get("seqSize")
						geneIndices.put (gene, lastEnding)

					}
				}
				buff << "\t;\n"
				buff << "end;\n"
				
				buff << "begin assumptions;\n"
					int start = 1;
					geneIndices.each {gene,ending ->
						buff <<	"\tcharset ${gene}. = ${start}-${ending};\n"
						start = ending+1;
					}
				
				buff << "end;"
				
				out.write buff.toString()
			}
			
			return [nexusOutFile]
		}
		else{
			log.error "Forming Nexus file for gene ${gene.name}: No output file, will not be sent."
		}
		
    }
	
	public static void printSixtyAtATime(StringBuffer buff, LinkedHashMap map,List taxaList){
		int seqLength = map.get("seqSize")
		taxaList.sort()

		for(int i = 0; i < seqLength; i+=60 ){
			int lowerbound = i;
			int upperbound = i+60;
			
			if(upperbound > seqLength){
				upperbound = seqLength;
			}
			taxaList.each{key->
				buff << "\t\t${key.name}\t${map.get(key.name).substring(lowerbound,upperbound).toUpperCase()}\n"
			}
			buff << "\n\n"
			
		}
	}
	
	public static File createTempDir() {
		final String baseTempPath = System.getProperty("java.io.tmpdir");
	
		Random rand = new Random();
		int randomInt = 1 + rand.nextInt();
	
		File tempDir = new File(baseTempPath + File.separator + "tempDir" + randomInt);
		if (tempDir.exists() == false) {
			tempDir.mkdir();
		}
	
		tempDir.deleteOnExit();
	
		return tempDir;
	}
}
