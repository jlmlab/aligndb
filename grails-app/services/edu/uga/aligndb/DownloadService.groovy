package edu.uga.aligndb

import java.io.File;
import java.util.Date;
import java.io.OutputStream;
import org.biojavax.bio.seq.RichSequence.IOTools
import org.biojava.bio.seq.DNATools;
import edu.uga.aligndb.Sequence;
import org.biojava.bio.seq.db.*


class DownloadService {
	def mailService
	def springSecurityService
	def muscleService
	
    static transactional = true
	
	def tempDir = "attachments"
	def mailGenomeSequences(String webRootDir, String email, String outputType, List taxaList) {
		def files=[];
		def mailSequencesTo = mailSequences.curry(email)
		try{
			def userDetails = springSecurityService.principal
			def loggedUser = User.get(userDetails.id)
			File userDir = new File(webRootDir, "/${tempDir}/${loggedUser.username}")
			userDir.deleteDir()
			userDir.mkdirs()
			for (taxa in taxaList){
				def fileName = "${webRootDir}/${tempDir}/${loggedUser.username}/${taxa.name}.fasta"
				File taxaFile = new File(fileName)
				OutputStream taxaIO =  taxaFile.newOutputStream()
				files << taxaFile
				SequenceDB db = new HashSequenceDB();
				def geno = Genome.findByTaxa(taxa)
				if (geno != null){
					org.biojava.bio.seq.Sequence dna = DNATools.createDNASequence(geno.sequence, taxa.name)
					db.addSequence(dna)
				}
				
				IOTools.writeFasta(taxaIO, db.sequenceIterator(),	null)
				taxaIO.flush()
			}
			
		}catch(Exception e){
			log.error e.toString()
			e.printStackTrace()
		}
		
		mailSequencesTo(files)
	}
    def mailGeneSequences(String webRootDir, String email, String outputType, List taxaList, List geneList) {
		def files = [];
		def mailSequencesTo = mailSequences.curry(email)
		try{
			def userDetails = springSecurityService.principal
			def loggedUser = User.get(userDetails.id)
			def userDir = new File(webRootDir, "/${tempDir}/${loggedUser.username}")
			userDir.deleteDir()
			userDir.mkdirs()
			//println "In downloadService. Got ${taxaList} and ${geneList}"
			for (gene in geneList){
				def fileName = "${webRootDir}/${tempDir}/${loggedUser.username}/${gene.name}.fasta"
				File geneFile = new File(fileName)
				OutputStream geneIO =  geneFile.newOutputStream()
				files << geneFile
				SequenceDB db = new HashSequenceDB();
				
				for (taxa in taxaList){
					//println "Taxa: ${taxa.name} Gene: ${gene.name}"
					
					def seq = Sequence.findByTaxaAndGene(taxa,gene)

					if (seq != null){
						
						org.biojava.bio.seq.Sequence dna = DNATools.createDNASequence(seq.sequence, taxa.name)
						db.addSequence(dna)
					}
				}
				IOTools.writeFasta(geneIO, db.sequenceIterator(),	null)
				geneIO.flush()
				
			}
		}catch(Exception e){
			log.error e.toString()
			e.printStackTrace()
		}
		if(outputType == 'fasta'){
			mailSequencesTo(files)
		}else if(outputType == 'afasta'){
			def alignedFiles = muscleService.doMuscleAlignment(taxaList, geneList, mailSequencesTo,false);
		}else{
			def alignedNexus = muscleService.doMuscleAlignment(taxaList, geneList, mailSequencesTo,true);
		}
		
		
    }
	
	def mailSequences = {String email,List files -> 
		mailService.sendMail{
			multipart true
			to email
			from "raj@plantbio.uga.edu"
			subject "Fasta files from AlignDB"
			body "Requested fasta files"
			for (theFile in files){
				attachBytes theFile.name, "chemical/seq-na-fasta", theFile.getBytes() //text/plain
			}
			 
		}

	}
}
