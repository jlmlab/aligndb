<!-- jkerry edit feb 8th 2011-->
<html>
	<head>
		<title><g:layoutTitle default="MonATol plastid gene database" /></title>
		<link rel="stylesheet" href="${resource(dir:'css',file:'main.css')}" />

		<link rel="shortcut icon" href="${resource(dir:'images',file:'favicon.ico')}" type="image/x-icon" />
		<blueprint:resources plugins="buttons, fancy-type" />
		<g:javascript library="jquery" plugin="jquery"/>
		<!--		
		<g:javascript library="application" />
		-->
		<g:layoutHead />
	</head>
	<body>
		<div id="spinner" class="spinner" style="display: none;"><img
			src="${resource(dir:'images',file:'spinner.gif')}"
			alt="${message(code:'spinner.alt',default:'Loading...')}" /></div>
		<div class="container">
			<div class="span-24" id="header">	
				<div id="grailsLogo">
					<a href="${createLink(uri: '/')}">
						<img src="${resource(dir:'images',file:'MonAToL-Plastid-logo.png')}" alt="MonAToL Plastid Genome Database"/>
					</a>
				</div>
			</div>
			<div class="span-24" id="sub-header">
				<h3 class="alt">A online resource to store and retrieve plastid gene sequences.</h3>
			</div>
			<div class="span-4 colborder">
				<sec:ifNotLoggedIn>
			    <h3 class="alt">Authentication</h3>
					<ul class="box alt" style="font-size: medium;">
						<li><g:link controller="login">Login</g:link></li>
						<li><g:link controller="register">Register</g:link></li>
					</ul>
				</sec:ifNotLoggedIn>
				<sec:ifLoggedIn>
				
			    <h3 class="alt">Links</h3>
	    		<ul class="box alt" style="font-size: medium;">
	    			<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
					
					<sec:ifAnyGranted roles="ROLE_SUPERADMIN,ROLE_ADMIN,ROLE_USER">
					<g:pageProperty name="page.menu-items" />
					<hr/>
					<legend>View</legend>
					<li><g:link controller="taxa">Species</g:link></li>
					<li><g:link controller="gene">Genes</g:link></li>
					<hr>
					<legend>Data</legend>
		    		<li><g:link controller="matrix">Download</g:link></li>
		    		</sec:ifAnyGranted>
					<sec:ifAnyGranted roles="ROLE_ADMIN,ROLE_SUPERADMIN">
						<li><g:link controller="upload">Upload</g:link></li>
					</sec:ifAnyGranted>
					<hr/>
					<li><g:link controller="logout">Logout</g:link></li>
					<li><g:link controller="user" action="myAccount">My Account</g:link></li>
					<sec:ifAnyGranted roles="ROLE_SUPERADMIN">
						<li><g:link controller="user" action="management">Manage Users</g:link></li>
					</sec:ifAnyGranted>
				</sec:ifLoggedIn>
				</ul>
			</div>
			<div class="span-19 last">
				<g:layoutBody />
			</div>
		</div>
	</body>
</html>
