<html>
    <head>
        <title>Welcome to MonATol</title>
        <meta name="layout" content="main" />
    </head>
   	<content tag="menu-items">
		<sec:ifNotLoggedIn>
		</sec:ifNotLoggedIn>
		<sec:ifLoggedIn>
		</sec:ifLoggedIn>
	</content>



    <body>
            <h1>Welcome to MonATol plastid gene database</h1>
            <p>This is an online resource to store and retrive your plastid gene sequences. If you would like an account please fill out the new user form and an administrator will activate your account shortly. For support information or to report a problem please contact <a href="mailto:john@uga.edu?subject=MonATol Plastid website support">John</a>. </p>
    </body>
</html>
