

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code='spring.security.ui.login.title'/></title>

        <style type="text/css">
        .hidden { display: none; }
        .link {cursor:pointer}
        </style>
        
    	<script type="text/javascript">
     		function reveal( idName ){       
         		$('.sequenceDiv').addClass('seqHidden');
         		$('#'+idName).removeClass('seqHidden');    
    		}
    		
     		function hide( idName ){       
         		$('.showHide').addClass('hidden');
     		}
    	</script>	
    </head>
    
   	<content tag="menu-items">
		<sec:ifNotLoggedIn>
		</sec:ifNotLoggedIn>
		<sec:ifLoggedIn>
			<li><g:link action="create">New ${entityName}</g:link></li>
		</sec:ifLoggedIn>
	</content>

    <body>
        <div class="body">
            <h1>Member Sign In</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="span-16 last" >	
            <p>
        		<div class="login s2ui_center ui-corner-all" style='text-align:center;'>
				<div class="login-inner">
				<form action='${postUrl}' method='POST' id="loginForm" name="loginForm" autocomplete='off'>
				<div class="sign-in">

				<table>
					<tr>
						<td><label for="username"><g:message code='spring.security.ui.login.username'/></label></td>
						<td><input name="j_username" id="username" size="20" /></td>
					</tr>
					<tr>
						<td><label for="password"><g:message code='spring.security.ui.login.password'/></label></td>
						<td><input type="password" name="j_password" id="password" size="20" /></td>
					</tr>
					<tr>
					<td colspan='2'>
						<input type="checkbox" class="checkbox" name="${rememberMeParameter}" id="remember_me" checked="checked" />
						<label for='remember_me'><g:message code='spring.security.ui.login.rememberme'/></label> |
						<span class="forgot-link">
						<g:link controller='register' action='forgotPassword'><g:message code='spring.security.ui.login.forgotPassword'/></g:link>
					</span>
					</td>
					</tr>
					<tr>
					<td colspan='2'>  
						<input type="submit" class="s2ui_hidden_button" id="loginButton_submit" value="Login"></input>
						 <!--<s2ui:submitButton elementId='loginButton' form='loginForm' messageCode='spring.security.ui.login.login'/>-->
					</td>
					</tr>
				</table>

				</div>
				</form>
				</div>
				</div>

				<script>
					$(document).ready(function() {
					$('#username').focus();
					});
				<s2ui:initCheckboxes/>
				</script>
        	</div>
        
    </body>
</html>
