
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main"/>
		<title><g:message code='spring.security.ui.register.title'/></title>

        <style type="text/css">
        .hidden { display: none; }
        .link {cursor:pointer}
        </style>
        
    	<script type="text/javascript">
		$(document).ready(function(){
			$('#details').focusin(function(){
				if(this.value == "Please enter some details concerning your institution affiliation and/or enter comments for the administrators confirming your application."){
					this.value = '';
				}
				else{
				
				}
	         });
			$('#details').focusout(function(){
				if(this.value == ''){
					this.value = this.defaultValue;
				}
				
	         });
         		
		});
		
     		
    	</script>	
    </head>

    <body>
        <div class="body">
            <h1>Create Account</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="span-16 last" >	
        		<s2ui:form width='650' height='300' elementId='loginFormContainer'
					titleCode='' center='true'>

				<g:form action='register' name='registerForm'>

				<g:if test='${emailSent}'>
				<br/>
				<b><g:message code='spring.security.ui.register.sent'/></b>
				</g:if>
				<g:else>

				<br/>

				<table>
				<tbody>

				<s2ui:textFieldRow name='firstName' labelCode='user.firstName.label' bean="${command}"
                         size='40' labelCodeDefault='Firstname' value="${command.firstName}"/>
				<s2ui:textFieldRow name='lastName' labelCode='user.lastName.label' bean="${command}"
                         size='40' labelCodeDefault='Lastname' value="${command.lastName}"/>
				<s2ui:textFieldRow name='username' labelCode='user.username.label' bean="${command}"
                         size='40' labelCodeDefault='Username' value="${command.username}"/>
				<s2ui:textFieldRow name='email' bean="${command}" value="${command.email}"
		                   size='40' labelCode='user.email.label' labelCodeDefault='E-mail'/>
				<s2ui:passwordFieldRow name='password' labelCode='user.password.label' bean="${command}"
                             size='40' labelCodeDefault='Password' value="${command.password}"/>
				<s2ui:passwordFieldRow name='password2' labelCode='user.password2.label' bean="${command}"
                             size='40' labelCodeDefault='Password (again)' value="${command.password2}"/>
                <tr class="prop">
					<td valign="top" class="name">
						<label for="email">Details</label>
					</td>
					<td valign="top" class="value ">
						<textarea type="text" name="details" value="" id="details" style="width:262px">${details}</textarea>
					</td>
				</tr>

				</tbody>
				</table>

				
				<input type='submit' value='Submit' id='create_submit' class='s2ui_hidden_button'></input>
				

				</g:else>

				</g:form>

				</s2ui:form>

        	</div>
        
    </body>
</html>
