
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title><g:message code='spring.security.ui.forgotPassword.title'/></title>
		<meta name='layout' content='main'/>

        <style type="text/css">
        .hidden { display: none; }
        .link {cursor:pointer}
        </style>
        
    	<script type="text/javascript">
     		function reveal( idName ){       
         		$('.sequenceDiv').addClass('seqHidden');
         		$('#'+idName).removeClass('seqHidden');    
    		}
    		
     		function hide( idName ){       
         		$('.showHide').addClass('hidden');
     		}
    	</script>	
    </head>
    
   	<content tag="menu-items">
		<sec:ifNotLoggedIn>
		</sec:ifNotLoggedIn>
		<sec:ifLoggedIn>
			<li><g:link action="create">New ${entityName}</g:link></li>
		</sec:ifLoggedIn>
	</content>

    <body>
        <div class="body">
            <h1>Forgot Password</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            
            <div class="span-16 last" >	
					<s2ui:form width='350' height='220' elementId='forgotPasswordFormContainer'
           titleCode='' center='true'>

	<g:form action='forgotPassword' name="forgotPasswordForm" autocomplete='off'>

	<g:if test='${emailSent}'>
	<br/>
	<b><g:message code='spring.security.ui.forgotPassword.sent'/></b>
	</g:if>

	<g:else>

	<br/>

	<table>
		<tr>
			<td><label for="username"><g:message code='spring.security.ui.forgotPassword.username'/></label></td>
			<td><g:textField name="username" size="25" /></td>
		</tr>
	</table>

	<s2ui:submitButton elementId='reset' form='forgotPasswordForm' messageCode='spring.security.ui.forgotPassword.submit'/>

	</g:else>

	</g:form>
</s2ui:form>

<script>
$(document).ready(function() {
	$('#username').focus();
});
</script>
        	</div>
        
    </body>
</html>
