

<%@ page import="edu.uga.aligndb.Sequence" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'sequence.label', default: 'Sequence')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
        <style type="text/css">
        	.hidden { display: none; }
        </style>
        
    	<script type="text/javascript">
     		function reveal( idName ){       
         		$('.sequenceDiv').addClass('seqHidden');
         		$('#'+idName).removeClass('seqHidden');    
    		}
    		
     		function hide( idName ){       
         		$('.showHide').addClass('hidden');
     		}
    	</script>
    </head>
    <body>
        <div class="body">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <div id="description" class="showHide">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean pharetra erat 
            turpis, et eleifend felis. Praesent tempor, dolor a eleifend vulputate, nisi nulla fermentum tortor, a vehicula purus 
            nisl placerat nibh. Sed interdum, est et mollis pellentesque, orci velit rutrum tellus, nec aliquam elit erat nec 
            enim. <a onclick="hide(description)">(hide description)</a>
            </div><br />
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${sequenceInstance}">
            <div class="errors">
                <g:renderErrors bean="${sequenceInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="taxa"><g:message code="sequence.taxa.label" default="Taxa" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: sequenceInstance, field: 'taxa', 'errors')}">
                                    <g:select name="taxa.id" from="${edu.uga.aligndb.Taxa.list()}" optionKey="id" value="${sequenceInstance?.taxa?.id}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="gene"><g:message code="sequence.gene.label" default="Gene" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: sequenceInstance, field: 'gene', 'errors')}">
                                    <g:select name="gene.id" from="${edu.uga.aligndb.Gene.list()}" optionKey="id" value="${sequenceInstance?.gene?.id}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="state"><g:message code="sequence.state.label" default="State" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: sequenceInstance, field: 'state', 'errors')}">
                                    <g:select name="state" from="${sequenceInstance.constraints.state.inList}" value="${sequenceInstance?.state}" valueMessagePrefix="sequence.state"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="theOne"><g:message code="sequence.theOne.label" default="The One" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: sequenceInstance, field: 'theOne', 'errors')}">
                                    <g:checkBox name="theOne" value="${sequenceInstance?.theOne}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="sequence"><g:message code="sequence.sequence.label" default="Sequence" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: sequenceInstance, field: 'sequence', 'errors')}">
                                    <g:textArea name="sequence" cols="40" rows="5" value="${sequenceInstance?.sequence}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="alignedSequence"><g:message code="sequence.alignedSequence.label" default="Aligned Sequence" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: sequenceInstance, field: 'alignedSequence', 'errors')}">
                                    <g:textArea name="alignedSequence" cols="40" rows="5" value="${sequenceInstance?.alignedSequence}" />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
