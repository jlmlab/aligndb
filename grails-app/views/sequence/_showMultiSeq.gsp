		<select name="sequenceSelection" id="sequenceSelector">
			<g:each in="${sequences}">
					<option value="seq${it.id}" <%if(it.theOne){print "selected='true'"}%> >${it.primaryUser?.username} - ${it.lastUpdated}</option>
			</g:each>
		</select>
		<hr />
		<g:each in="${sequences}" var="theSeq">
				<g:render template="showSeq" model="['multiGen':true,'sequenceInstance':theSeq,'currentID':currentID]" />
		</g:each>