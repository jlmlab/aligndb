
<%@ page import="edu.uga.aligndb.Sequence" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'sequence.label', default: 'Sequence')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>

    <body>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'sequence.id.label', default: 'Id')}" />
                        
                            <th><g:message code="sequence.taxa.label" default="Taxa" /></th>
                        
                            <th><g:message code="sequence.gene.label" default="Gene" /></th>
                        
                            <g:sortableColumn property="state" title="${message(code: 'sequence.state.label', default: 'State')}" />
                        
                            <g:sortableColumn property="theOne" title="${message(code: 'sequence.theOne.label', default: 'The One')}" />
                        
                            <g:sortableColumn property="sequence" title="${message(code: 'sequence.sequence.label', default: 'Sequence')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${sequenceInstanceList}" status="i" var="sequenceInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${sequenceInstance.id}">${fieldValue(bean: sequenceInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: sequenceInstance, field: "taxa")}</td>
                        
                            <td>${fieldValue(bean: sequenceInstance, field: "gene")}</td>
                        
                            <td>${fieldValue(bean: sequenceInstance, field: "state")}</td>
                        
                            <td><g:formatBoolean boolean="${sequenceInstance.theOne}" /></td>
                        
                            <td>${fieldValue(bean: sequenceInstance, field: "sequence").substring(0,10)}...</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${sequenceInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
