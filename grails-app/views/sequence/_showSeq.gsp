<g:if test="${multiGen}">
	<g:if test="${sequenceInstance.theOne}">
		<div id="seq${sequenceInstance.id}" class="seqDiv">
	</g:if>
	<g:else>
		<div id="seq${sequenceInstance.id}" class="hidden seqDiv">
	</g:else>
</g:if>
<g:else>
	<div id="seq${sequenceInstance.id}" class="seqDiv">
</g:else>

            <div class="dialog">
                <table>
                    <tbody>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="sequence.id.label" default="Id" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: sequenceInstance, field: "id")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="sequence.taxa.label" default="Taxa" /></td>
                            
                            <td valign="top" class="value"><g:link controller="taxa" action="show" id="${sequenceInstance?.taxa?.id}">${sequenceInstance?.taxa?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="sequence.gene.label" default="Gene" /></td>
                            
                            <td valign="top" class="value"><g:link controller="gene" action="show" id="${sequenceInstance?.gene?.id}">${sequenceInstance?.gene?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="sequence.state.label" default="State" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: sequenceInstance, field: "state")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="sequence.theOne.label" default="Canonical" /></td>
                            
                            <td valign="top" class="value"><g:formatBoolean boolean="${sequenceInstance?.theOne}" /></td>
                            
                        </tr>
                        
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="sequence.theOne.label" default="Primary User" /></td>
                            
                            <td valign="top" class="value">${sequenceInstance?.primaryUser?.username}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="sequence.sequence.label" default="Sequence" /></td>
                            
                            <td valign="top" class="value"  style="font-family: monospace; font-size: medium"><g:seqRenderer size="65" seq="${sequenceInstance.sequence}" /></td>
                            
                        </tr>
                    
                    	<g:if test="${sequenceInstance.alignedSequence != null}">
	                        <tr class="prop">
	                            <td valign="top" class="name"><g:message code="sequence.alignedSequence.label" default="Aligned Sequence" /></td>
	                            
	                            <td valign="top" class="value">${fieldValue(bean: sequenceInstance, field: "alignedSequence")}</td>
	                            
	                        </tr>
                    	</g:if>
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="sequence.dateCreated.label" default="Date Created" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${sequenceInstance?.dateCreated}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="sequence.lastUpdated.label" default="Last Updated" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${sequenceInstance?.lastUpdated}" /></td>
                            
                        </tr>
                    
                    </tbody>
                </table>
            </div>
            <sec:ifAnyGranted roles="ROLE_SUPERADMIN">
            	<div class="buttons">
                	<g:form>
	                    <g:hiddenField name="id" value="${sequenceInstance?.id}" />
	                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'default.button.edit.label', default: 'Edit')}" /></span>
	                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
               		</g:form>
            	</div>
            </sec:ifAnyGranted>
            <sec:ifNotGranted roles="ROLE_SUPERADMIN">
            	<g:if test="${ sequenceInstance?.primaryUser.id == currentID}">
            		<div class="buttons">
                		<g:form>
	                    	<g:hiddenField name="id" value="${sequenceInstance?.id}" />
	                    	<span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'default.button.edit.label', default: 'Edit')}" /></span>
	                    	<span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
               	 		</g:form>
            		</div>
            	</g:if>
            </sec:ifNotGranted>
            	
            
            </div>
</div>
