

<%@ page import="edu.uga.aligndb.Sequence" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'sequence.label', default: 'Sequence')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>

    <body>
        <div class="body">
            <h1><g:message code="default.edit.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${sequenceInstance}">
            <div class="errors">
                <g:renderErrors bean="${sequenceInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${sequenceInstance?.id}" />
                <g:hiddenField name="version" value="${sequenceInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="taxa"><g:message code="sequence.taxa.label" default="Taxa" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: sequenceInstance, field: 'taxa', 'errors')}">
                                    <g:select name="taxa.id" from="${edu.uga.aligndb.Taxa.list()}" optionKey="id" value="${sequenceInstance?.taxa?.id}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="gene"><g:message code="sequence.gene.label" default="Gene" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: sequenceInstance, field: 'gene', 'errors')}">
                                    <g:select name="gene.id" from="${edu.uga.aligndb.Gene.list()}" optionKey="id" value="${sequenceInstance?.gene?.id}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="state"><g:message code="sequence.state.label" default="State" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: sequenceInstance, field: 'state', 'errors')}">
                                    <g:select name="state" from="${sequenceInstance.constraints.state.inList}" value="${sequenceInstance?.state}" valueMessagePrefix="sequence.state"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="theOne"><g:message code="sequence.theOne.label" default="The One" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: sequenceInstance, field: 'theOne', 'errors')}">
                                    <g:checkBox name="theOne" value="${sequenceInstance?.theOne}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="sequence"><g:message code="sequence.sequence.label" default="Sequence" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: sequenceInstance, field: 'sequence', 'errors')}">
                                    <g:textArea name="sequence" cols="40" rows="5" value="${sequenceInstance?.sequence}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="alignedSequence"><g:message code="sequence.alignedSequence.label" default="Aligned Sequence" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: sequenceInstance, field: 'alignedSequence', 'errors')}">
                                    <g:textArea name="alignedSequence" cols="40" rows="5" value="${sequenceInstance?.alignedSequence}" />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
