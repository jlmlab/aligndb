<%@ page import="edu.uga.aligndb.Sequence" %>
<%@ page import="org.grails.comments.*" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'sequence.label', default: 'Sequence')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
        
        <g:if test="${multi}">
        <script type="text/javascript">
     		function reveal( idName ){       
         		$('.sequenceDiv').addClass('seqHidden');
         		$('#'+idName).removeClass('seqHidden');    
    		}
    		
     		function hide( idName ){       
         		$('.showHide').addClass('hidden');
     		}
     		function reveal(id){
				$('div.seqDiv').addClass('hidden');
				$('div#'+id).removeClass('hidden');
     		}
			
         	//document javascript binding
         	$(document).ready(function(){
         		reveal($('#sequenceSelector').val());
         		$('#sequenceSelector').change(function(){
					reveal($('#sequenceSelector').val());
             	});
             });
    	</script>
    	</g:if>
    </head>
    

    <body>
    	<g:if test="${multi}">
	        <div class="body">
	            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
	            <g:if test="${flash.message}">
	            <div class="message">${flash.message}</div>
	            </g:if>
	            <g:render template="showMultiSeq" model="['sequences':sequences]" />
	        </div>
        </g:if>
        <g:else>
	        <div class="body">
	            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
	            <g:if test="${flash.message}">
	            <div class="message">${flash.message}</div>
	            </g:if>
	            <g:render template="showSeq" model="['multiGen':false,'sequenceInstance':sequenceInstance,'currentID':currentID]" />
	        </div>
        </g:else>
    </body>
</html>