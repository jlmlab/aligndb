 
<%@ page import="edu.uga.aligndb.Gene" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'upload.label', default: 'Upload Gene/Taxa file')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
        <style type="text/css">
        .hidden { display: none; }
        .createNew{
			background: #fdffde;	
		}
		.existing{
			background: #AAF99F;
		}
        </style>
        
    	<script type="text/javascript">
    		var mcId = 0;
 			var newMC = false;
 			var mcName = "";
 			
 			var tOId = 0;
 			var newTO = false;
 			var tOName = "";
 			
 			var famId = 0;
 			var newFam = false;
 			var famName = "";

 			var taxaName = "";


 			function sendToNTCAction(){

 				$.ajax({
 	 				url:"${createLink(action:'newTaxaChain')}",
 	 				data:{batchData:true,mcId:mcId,newMC:newMC,mcName:mcName,tOId:tOId,newTO:newTO,tOName:tOName,famId:famId,newFam:newFam,famName:famName,taxaName:taxaName},
 	 				success:function(data){
						$('div.body').children().remove();
						$('div.body').css('text-align','center');
						$('div.body').append("<a href='${createLink(action:'index')}' style='text-decoration:none; padding:5px;border:1px solid #C0C0C0;'>Proceede to upload page</a>");
 	 				},
 	 				failure:function(data){
						alert(data);
 	 				}
 						});
 	 	 			
 			}
     		//MC select to tO select
     		function getTaxaOrderList(){
				var selectBox = $('#mCSelect');
				var textBoxContents = $('#mcText').val().replace(/ /g,'_');
				
				if(textBoxContents==""&&$('#mCSelect').val() != "null"){
					newMC = false;
					mcId = $('#mCSelect').val();
					mcName = $('#mCSelect :selected').text();
				}
				else if(textBoxContents!=""){
					newMC = true;
					mcId = 0;
					mcName = textBoxContents;
				}
				else{
					alert("Please select a higher taxon or create a new higher taxon.");
				}
				
				$.ajax({url:"${createLink(controller:'taxaOrder',action:'list')}",
						data:{mcId:mcId,newMC:newMC,mcName:mcName,form:true},
						success:function(famList){
									if(famList=="<p>Error: Duplicate mcName</p>"){
										alert("You have entered a higher taxon name which already exists in the database.  Please enter a name for a new higher taxa or select your intended higher taxa from the dropdown menu.");
									}
									else{
										$("#formContents").fadeOut('fast',function(){
											$("#higherTaxons").remove();
											$("#formLegend").text("Step 2: Select Order")
											$('#formContents').append(famList);
											$("#formContents").fadeIn('fast');
										});
										placeSelectedUI();
									}
								},
						failure:function(err){}
						});
				
     		}

			function backToMCSelect(){
				mcId = 0;
	 			newMC = false;
	 			mcName = "";
	 			
				$.ajax({url:"${createLink(controller:'majorClassification',action:'list')}",
					data:{form:true},
					success:function(mcList){
									
									$("#formContents").fadeOut('fast',function(){
										$("#Orders").remove();
										$("#formLegend").text("Step 1: Select Higher Taxon")
										$('#formContents').append(mcList);
										$("#formContents").fadeIn('fast');
										
									});
									placeSelectedUI();
								
							},
					failure:function(err){}
					});
			}
			//END MC Select to tO Select
			
			//tO Select to Fam Select
     		function getFamilyList(){

				var textBoxContents = $('#tOText').val().replace(/ /g,'_');
				
				if(textBoxContents==""&&$('#tOSelect').val() != "null"){
					newTO = false;
					tOId = $('#tOSelect').val();
					tOName = $('#tOSelect :selected').text();
				}
				else if(textBoxContents!=""){
					newTO = true;
					tOId = 0;
					tOName = textBoxContents;
				}
				else{
					alert("Please select an order or create a new order.");
				}
				
				$.ajax({url:"${createLink(controller:'family',action:'list')}",
						data:{tOId:tOId,newTO:newTO,tOName:tOName,form:true},
						success:function(famList){
									if(famList=="<p>Error: Duplicate tOName</p>"){
										alert("You have entered an order name which already exists in the database.  Please enter a name for a new order or select your intended order from the dropdown menu.");
									}
									else{
										$("#formContents").fadeOut('fast',function(){
											$("#Orders").remove();
											$("#formLegend").text("Step 3: Select a Family")
											$('#formContents').append(famList);
											$("#formContents").fadeIn('fast');
										
										});
										placeSelectedUI();
									}
								},
						failure:function(err){alert('error');}
						});
				
     		}
     		function backToTOSelect(){
				tOId = 0;
	 			newTO = false;
	 			tOName = "";
	 			
				$.ajax({url:"${createLink(controller:'taxaOrder',action:'list')}",
					data:{mcId:mcId,newMC:newMC,mcName:mcName,form:true},
					success:function(mcList){
									
									$("#formContents").fadeOut('fast',function(){
										$("#Families").remove();
										$("#formLegend").text("Step 2: Select an Order")
										$('#formContents').append(mcList);
										$("#formContents").fadeIn('fast');
										
									});
									placeSelectedUI();
								
							},
					failure:function(err){}
					});
			}

     		//END tO Select to Fam Select
     		
     		function getTaxaList(){

				var textBoxContents = $('#famText').val().replace(/ /g,'_');
				
				if(textBoxContents==""&&$('#famSelect').val() != "null"){
					newFam = false;
					famId = $('#famSelect').val();
					famName = $('#famSelect :selected').text();
				}
				else if(textBoxContents!=""){
					newFam = true;
					famId = 0;
					famName = textBoxContents;
				}
				else{
					alert("Please select a family or create a new family.");
				}
				
				$.ajax({url:"${createLink(controller:'taxa',action:'verifyExistance')}",
						data:{famId:famId,newFam:newFam,famName:famName,form:true},
						success:function(taxForm){
									if(taxForm=="<p>Error: Duplicate famName</p>"){
										alert("You have entered a family name which already exists in the database.  Please enter a name for a new family or select your intended family from the dropdown menu.");
									}
									else{
										$("#formContents").fadeOut('fast',function(){
											$("#Families").remove();
											$("#formLegend").text("Step 4: Create a new Taxa");
											$('#formContents').append(taxForm);
											$("#formContents").fadeIn('fast');
										
										});
										placeSelectedUI();
									}
								},
						failure:function(err){alert('error');}
						});
				
     		}

     		function backToFamSelect(){
				famId = 0;
	 			newFam = false;
	 			famName = "";
	 			
	 			$.ajax({url:"${createLink(controller:'family',action:'list')}",
					data:{tOId:tOId,newTO:newTO,tOName:tOName,form:true},
					success:function(famList){
							
								$("#formContents").fadeOut('fast',function(){
									$("#taxa").remove();
									$("#formLegend").text("Step 3: Select a Family")
									$('#formContents').append(famList);
									$("#formContents").fadeIn('fast');
									
								});
								placeSelectedUI();
							},
					failure:function(err){alert('error');}
					});
			}
     		//Finalize and send, then redirect
     		function confirmAddition(){
     			var textBoxContents = $('#taxText').val().replace(/ /g,'_');
         		taxaName = textBoxContents;
     			$("#modificationForm").fadeOut('fast',function(){
					$("#taxa").addClass('hidden');
					$('#submit').removeClass('hidden');
					$('#divTableRule').addClass('hidden');
					});
     			placeSelectedUI();
     			$('#submit').removeClass('hidden');
     		}

     		

     		//UI Function
     		function deselect(iDName){
         		$('#'+iDName+' :selected').attr('selected', false);
     		}
     		function empty_Text(iDName){
     			$('#'+iDName).val("");
     		}
     		function hide( idName ){       
         		$('.showHide').addClass('hidden');
     		}
     		function placeSelectedUI(){

         		if(mcName != ""){
						$('#mcDisplay').text(mcName);
						if(newMC){
							$('#mcDisplayAction').addClass('createNew');
							$('#mcDisplayAction').text('Create New');
						}
						else{
							$('#mcDisplayAction').addClass('existing');
							$('#mcDisplayAction').text('Existing');
						} 						
         		}
         		else{
         			$('#mcDisplay').text("");
         			$('#mcDisplayAction').text("");
         			$('#mcDisplayAction').removeClass('existing');
         			$('#mcDisplayAction').removeClass('createNew');
         		}
         		if(tOName != ""){
         			$('#tODisplay').text(tOName);
         			
					if(newTO){
						$('#tODisplayAction').addClass('createNew');
						$('#tODisplayAction').text('Create New');
					}
					else{
						$('#tODisplayAction').addClass('existing');
						$('#tODisplayAction').text('Existing');
					} 						
     			}
     			else{
     				$('#tODisplay').text("");
     				$('#tODisplayAction').text("");
     				$('#tODisplayAction').removeClass('existing');
     				$('#tODisplayAction').removeClass('createNew');
     			}
         		if(famName != ""){
         			$('#famDisplay').text(famName);
					if(newFam){
						$('#famDisplayAction').addClass('createNew');
						$('#famDisplayAction').text('Create New');
					}
					else{
						$('#famDisplayAction').addClass('existing');
						$('#famDisplayAction').text('Existing');
					} 						
     			}
     			else{
     				$('#famDisplay').text("");
     				$('#famDisplayAction').text("");
     				$('#famDisplayAction').removeClass('existing');
     				$('#famDisplayAction').removeClass('createNew');
     			}
         		if(taxaName != ""){
					$('#taxaDisplay').text(taxaName);
					$('#taxaDisplayAction').addClass('createNew');
					$('#taxaDisplayAction').text('Create New'); 						
     			}
         		else{
         			$('#taxaDisplay').text("");
         			$('#taxaDisplayAction').text("");
         			$('#taxaDisplayAction').removeClass('createNew');
         		}

     		}
     		//END UI Function
    	</script>
    </head>
        <content tag="menu-items">
    	</content>
    <body>
        <div class="body">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            
            <div id="description" class="showHide">
            Use this form to create a new combination of Higher Taxons, Orders, Families, and taxa. Each selection is handled in order to ensure that your new entries fit into the existing <a href="${createLink(controller:'taxa',action:'list') }">taxa lineage tree</a>.<a onclick="hide(description)">(hide description)</a> <br />
            </div><br />
            
            <g:if test="${flash.message}">
	            <div class="error">${flash.message}</div>
	         
            </g:if>
            	<fieldset>
	        		<legend>Selected Lineage</legend>
	        		<div id="modificationForm">
	        			<h3 id="formLegend">Step 1: Select Higher Taxon</h3>
	        			<div id="formContents">
	        				<g:render template="../majorClassification/MajorClassificationSelectTemplate" model="['mcList':mcList]" />
	        			</div>
	        		</div>
	        		<hr id="divTableRule" />
	        		<table>
	        			<tr>
	        				<th>Level</th><th>Selection</th><th>Action</th>
	        			</tr>
	        			<tr>
	        				<td>Higher Taxon</td><td id="mcDisplay"></td><td id="mcDisplayAction"></td>
	        			</tr>
	        			<tr>
	        				<td>Order</td><td id="tODisplay"></td><td id="tODisplayAction"></td>
	        			</tr>
	        			<tr>
	        				<td>Family</td><td id="famDisplay"></td><td id="famDisplayAction"></td>
	        			</tr>
	        			<tr>
	        				<td>Taxa</td><td id="taxaDisplay"></td><td id="taxaDisplayAction"></td>
	        			</tr>
	        		</table>
	        		
	        		
	        		<button id="submit" class="hidden" onclick="sendToNTCAction()">Submit</button>
	        		
	        	</fieldset>
	        	
	        	

        </div>
        
    </body>
</html>
