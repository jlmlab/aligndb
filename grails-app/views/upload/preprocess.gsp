<%@ page import="edu.uga.aligndb.Gene" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'upload.label', default: 'Upload Gene/Taxa file')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
        <link rel="stylesheet" href="${resource(dir:'css',file:'Upload/preprocess.css')}" />
        <g:javascript src="jquery.escape.js"/>
    <script type="text/javascript">
	
	var state = 'none';
	var ambiguousDet = 'This gene does not yet exist in the database.  Submitting this sequence will result in a new gene/taxa possibility for each taxa. If this is not in error please select \'Add It\' below. If there is an error in your file and this is not a new gene please use one of the other available options to resolve the conflict.';
	var duplicateDet = 'There are two genes in your input with the same name. Please use one of the options below to resolve this conflict.';

	function addSequence(idName){
		$('.errorExp').remove();
		$('.sequenceDiv').addClass('seqHidden');
		$('#'+$.escape(idName)).removeClass('ambiguous');
		$('#'+$.escape(idName)).removeClass('both');
		$('#'+$.escape(idName)+'-seqStringTextarea').attr('readonly','yes');
		populateField()
	}

	function deleteSequence(idName,geneName){
		$('.errorExp').remove();
		$('.sequenceDiv').addClass('seqHidden');
		if($('#'+idName).hasClass('duplicate')){
			if($('.'+$.escape(geneName)+'-dup').size()==2){
				$('.'+$.escape(geneName)+'-dup').removeClass('duplicate');
				$('.'+$.escape(geneName)+'-dup').removeClass('both');
			}
		}
		$('#'+$.escape(idName)).remove();
		
		populateField()
	}

	function changeSequenceName(idName){
		$('.sequenceDiv').addClass('seqHidden');
		var geneName = $('#'+$.escape(idName)+'-gN').text();
		var inputText = $('#'+$.escape(idName)+'-newNameInput').val();
		$('#'+$.escape(idName)+'-gN').text(inputText);
		if($('#'+$.escape(idName)).hasClass('duplicate')){
			alert(geneName);
			if($('.'+$.escape(geneName)+'-dup').size()==2){
				$('.'+$.escape(geneName)+'-dup').removeClass('duplicate');
				$('.'+$.escape(geneName)+'-dup').removeClass('both');
				$('.'+$.escape(geneName)+'-dup').removeClass(geneName+'-dup');
			}
			else{
				$('#'+$.escape(idName)).removeClass('duplicate');
				$('#'+$.escape(idName)).removeClass('both');
				$('#'+$.escape(idName)).removeClass(geneName+'-dup');
			}
		}
		if($('#'+$.escape(idName)).hasClass('ambiguous')){
			$('#'+$.escape(idName)).removeClass('ambiguous');
			$('#'+$.escape(idName)).removeClass('both');
		}
		checkForAmbiguity(idName,inputText);
		checkForDuplicates(idName);
		
		$('.errorExp').remove();
		populateField()
	}

	function checkForAmbiguity(idName,inputText){

		$.ajax({ 
			url: "${createLink(action:'checkAmbiguity')}",
			data:{geneName:inputText},
			success: function( tfReturn ){
				if(tfReturn=='false'){
					$('#'+$.escape(idName)).addClass('ambiguous');
					if($('#'+$.escape(idName)).hasClass('duplicate')){
						$('#'+$.escape(idName)).addClass('both');
					}
				}
			},
			error: function( msg ){ alert('There was an error checking for ambiguity in the database.');}
		});
		
	}

	function checkForDuplicates(idName){
		var matchingElements = new Array();
		var examineGeneName = $('#'+$.escape(idName)+'-gN').text();
		$.each($('.geneName'),function (index,value){
			if($(value).text()==examineGeneName){
				matchingElements.push(value);
			}
		});
		if($(matchingElements).size() > 1){
			$.each(matchingElements,function (index,value){
				var valueP = $(value).parent();
				$(valueP).addClass('duplicate');
				$(valueP).addClass(examineGeneName+'-dup');
				if($(valueP).hasClass('ambiguous')){
					$(valueP).addClass('both');
				}
			});
		}
		else{
			var value = matchingElements[0];
			var valueP = $(value).parent();
			$(valueP).removeClass('duplicate');
			$(valueP).removeClass('both');
		}
		populateField()
	}
	 
	 function reveal( idName , geneName){
		$('.errorExp').remove();
		$('.sequenceDiv').addClass('seqHidden');
		$('#'+$.escape(idName)+'-seqString').removeClass('seqHidden');
		if($('#'+$.escape(idName)).hasClass('ambiguous')){
			$('#'+$.escape(idName)).append('<h5 class="errorExp" id="'+$.escape(idName)+'-ambiguousDet">'+ambiguousDet+'</h5>');
			$('#'+$.escape(idName)).append('<div class="errorExp">');
				$('#'+$.escape(idName)).append('<hr class="errorExp"/>');
				$('#'+$.escape(idName)).append('<h6 class="errorExp">Add a New Gene to the Database:</h6>');
				$('#'+$.escape(idName)).append('<input type="submit" class="errorExp" value="Add It" onclick="addSequence(\''+idName+'\')" />');
				$('#'+$.escape(idName)).append('<hr class="errorExp"/>');
				$('#'+$.escape(idName)).append('<h6 class="errorExp">Delete This Sequence:</h6>');
				$('#'+$.escape(idName)).append('<input type="submit" class="errorExp" value="Delete It" onclick="deleteSequence(\''+idName+'\',\''+geneName+'\')" />');
				$('#'+$.escape(idName)).append('<hr class="errorExp"/>');
				$('#'+$.escape(idName)).append('<h6 class="errorExp">Change the Sequence Name:</h6>');
				$('#'+$.escape(idName)).append('<input id="'+idName+'-newNameInput" class="errorExp" type="text" value="new sequence name" /><br class="errorExp"/><input class="errorExp" type="submit" value="Change It" onclick="changeSequenceName(\''+idName+'\')"/>');
			$('#'+$.escape(idName)).append('</div>');
		}
		if($('#'+$.escape(idName)).hasClass('duplicate')){
			$('#'+$.escape(idName)).append('<h5 class="errorExp" id="'+idName+'-duplicateDet">'+duplicateDet+'</h5>');
			$('#'+$.escape(idName)).append('<div class="errorExp">');
				$('#'+$.escape(idName)).append('<hr class="errorExp"/>');
				$('#'+$.escape(idName)).append('<h6 class="errorExp">Delete This Sequence:</h6>');
				$('#'+$.escape(idName)).append('<input type="submit" class="errorExp" value="Delete It" onclick="deleteSequence(\''+idName+'\',\''+geneName+'\')" />');
				$('#'+$.escape(idName)).append('<hr class="errorExp"/>');
				$('#'+$.escape(idName)).append('<h6 class="errorExp">Change the Sequence Name:</h6>');
				$('#'+$.escape(idName)).append('<input  id="'+idName+'-newNameInput" class="errorExp" type="text" value="new sequence name" /><br class="errorExp"/><input class="errorExp" type="submit" value="Change It" onclick="changeSequenceName(\''+idName+'\')"/>');
			$('#'+$.escape(idName)).append('</div>');
		}
    }

	function populateField(){
		var jsonText = '{';
		jsonText += "  'taxaName'  :  '"+$('.taxaName').attr('id')+"'  ";
		var sendit = true;
		$.each($('.wholeSet'),function (index,value){

			if($(value).hasClass('ambiguous')||$(value).hasClass('duplicate')){
				sendit = false;
			}
		});
		var count = 1;
		$.each($('.wholeSet'),function (index,value){
			var idName = $(value).attr('id');
			var map = ",'map"+count+"':{";
			var geneName = $('#'+$.escape(idName)+'-gN').text();
			map+=" 'geneName' : '"+geneName+"'," ;
			var seqString = $('#'+$.escape(idName)+'-seqStringTextarea').val();
			map+=" 'seqString' : '"+seqString+"'}"
			jsonText += map;
			count++;
		});
		jsonText+='}';
		$('#finalTextInput').val(jsonText);
		if(sendit){
			$('#sub').removeClass('hidden');
		}
		else{
			$('#sub').addClass('hidden');
		}
		
		
	}

	$(document).ready(function() {
		populateField();
	});

    </script>
    </head>
        <content tag="menu-items">
        </content>
    <body>
      <g:if test="${seqList}" != null>
        <div class="body">
            <h1>Upload Verification</h1>
            <!-- use a g:each like this to iterate through the maps, note the var=map line. -->
            <p>Please verify that the sequences you wish to upload are correct. Please click any cells that are colored to resolve a conflict with the database.</p>
            <h4>All conflicts must be resolved before your sequences are submitted to the database.</h4>
            <div id="seqList">
            <g:each var="map" in="${seqList}">
            <g:if test="${map.key == 'taxaName'}">
            	<span style="display:none;" id="${map.value }" class="${map.key }"></span>
            </g:if>
            <g:else>
            
                <g:if test="${map.value['ambiguous'] && !map.value['duplicate']}">
                <div id="${map.key}" class="wholeSet ambiguous">
                    <div id="${map.key}-gN" class="geneName"   onclick="reveal('${map.key}','${map.value['ganeName']}');">${map.value['geneName']}</div>
                    <div id="${map.key}-seqString" class="sequenceDiv seqHidden">
						<textarea name="comments" id="${map.key}-seqStringTextarea" cols="40" rows="5">${map.value['seqString']}</textarea>
					</div> 
                </div>   
                </g:if>
                <g:elseif test="${map.value['duplicate'] && !map.value['ambiguous']}" env="development">
                <div id="${map.key}" class="wholeSet duplicate ${map.value['geneName']}-dup">
                    <div id="${map.key}-gN" class="geneName"  onclick="reveal('${map.key}','${map.value['geneName']}');">${map.value['geneName']}</div>
                    <div id="${map.key}-seqString" class="sequenceDiv seqHidden">
						<textarea name="comments" id="${map.key}-seqStringTextarea" cols="40" rows="5">${map.value['seqString']}</textarea>
					</div>  
                </div>   
                </g:elseif>
                <g:elseif test="${map.value['duplicate'] && map.value['ambiguous']}" env="development duplicate ambiguous">
                <div id="${map.key}" class="wholeSet duplicate ambiguous both ${map.value['geneName']}-dup">
                    <div id="${map.key}-gN" class="geneName" style="background: #FF5333"  onclick="reveal('${map.key}','${map.value['geneName']}');">${map.value['geneName']}</div>
                    <div id="${map.key}-seqString" class="sequenceDiv seqHidden">
						<textarea name="comments" id="${map.key}-seqStringTextarea" cols="40" rows="5">${map.value['seqString']}</textarea>
					</div>
                </div> 
                  </g:elseif>
                <g:else>
                <div id="${map.key}" class="wholeSet">
                    <div id="${map.key}-gN" class="geneName"  onclick="reveal('${map.key}','${map.value['geneName']}');">${map.value['geneName']}</div>
                    <div id="${map.key}-seqString" class="sequenceDiv seqHidden"><textarea name="noneditSeq" id="${map.key}-seqStringTextarea" cols="40" rows="5" readonly="yes">${map.value['seqString']}</textarea></div>   
                </div>               
                </g:else>
            </g:else>
            </g:each>
            <g:uploadForm action="dataPosted">
	        		<g:textField name="jsonText" class="text" id="finalTextInput" style="display:none;"/>
        			<g:submitButton  name="nsubmit" value="Submit" id="sub" class="hidden"/>
            </g:uploadForm>
            </div>
         </g:if>
          
         <g:elseif test="${genomeObj}" != null>
         	    <div class="body">
	         	<g:set var="entityName" value="${message(code: 'upload.label', default: 'Upload Genome File')}" />
	         	<h1><g:message code="default.show.label" args="[entityName]" /></h1>
	         	<p>An updated version of the genome for ${genomeObj['taxaName']} has been uploaded.</p>
	         		<%-- <div id="genomeObj">
	         			<g:uploadForm action="dataPosted" params="[genome:true]">
	        				<g:textField name="genomeObj" value="genomeObj" class="text" style="display:none;"/>
        					<g:submitButton  name="nsubmit" value="Submit" id="sub" class="hidden"/>
           				</g:uploadForm>
	         		</div>--%>
         		</div>
         </g:elseif>
            
            <p id="results"></p>
            <div class="commentCollection span-16 last" style="display:none">
        		<div id="commentForm">
        			<h4>Comments are listed below.  Please post the text you would use to describe the function of this page to your peers.  We want to describe the interface in such a way that it will make sense to biologists (and not just us computer scientists). If you have any questions please contact John at <a href="mailto:john@kerryhouse.net">john@kerryhouse.net</a></h4>
       		
        			<g:each var="comment" in="${comList}">
        				<div class="comment">${comment.value.body} -Posted By: ${comment.value.poster.username}</div>
        			</g:each>
        		
        			<g:form action="newComment">
        				<p>New Comment:</p>
        				<g:textArea name="viewName" value="preprocess" class="hidden" />
        				<g:textArea name="controllerName" value="${this.controllerName}" class="hidden" />
        				<g:textArea name="commentText" class="span_6"/>
        				<br />
        				<g:submitButton name="newComment" value=" Add Comment" />
        			</g:form>
        		</div>
        	</div>
        </div>
    </body>
</html> 