
<%@ page import="edu.uga.aligndb.Gene" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'newUpload.label', default: 'Upload Gene/Taxa file')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
        <content tag="menu-items">
        </content>
   	<g:jsonForm  action="getJSON">
        	<fieldset>
        		<legend>Dispay JSON Object</legend>
        		<p>
        			<label for="titleName">Name:</label>
        			<g:textField name="titleName" class="text" />
        		</p>
        		<p>
        			<label for="seqfile">File Name:</label>
        			<input type="file" name="seqfile" />
       			</p>
       			<p>
       				<label for="uploadType">Upload type:</label>
       				<g:radioGroup name="uploadType" labels="['Gene File','Taxa File']" values="[1,2]" value="1">
						<p>${it.label} ${it.radio}</p>
					</g:radioGroup>
       			</p>
       			<p>
       				<label for="overWrite">Overwrite existing sequence data:</label>
       				<g:checkBox name="overWrite" value="${false}"></g:checkBox>
       			</p>
       			<g:submitButton  name="submit" value="DummySubmit" />
        	</fieldset>
           </g:jsonForm>
</html>
