 
<%@ page import="edu.uga.aligndb.Gene" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'upload.label', default: 'Upload Gene/Species file')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
        <style type="text/css">
        .hidden { display: none; }
 		.link {cursor:pointer}
        </style>
        
    	<script type="text/javascript">
     		function hide( idName ){       
         		$('.showHide').addClass('hidden');
     		}
    	</script>
    </head>
        <content tag="menu-items"></content>
    <body>
        <div class="body">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            
            <div id="description" class="showHide">
            Use this form to upload a fasta file containing sequences for many genes in a single species. Select your species from the list below or click 'new species' to insert a new species into the established <a href="${createLink(controller:'taxa',action:'list') }">species lineage tree</a>.<a onclick="hide(description)" class="link">(hide description)</a> <br />
            </div><br />
            
            <g:if test="${flash.message}">
	            <div class="error">${flash.message}</div>
	         
            </g:if>
            <g:uploadForm  action="preprocess">
	        	<fieldset>
	        		<legend>Upload File</legend>
	        		<table>
	        			<tr>
	        		
	        				<td>
	        					Select the species from which your sequences are derived
	        				</td>
	        				<td>
	        					or create a new Species.
	        				</td>
	        				<tr>
	        					<td>
	        						<g:select name="titleName" from="${taxa}"/>
	        					</td>
	        					<td>
	        						<a href="${createLink(action:'newTaxaChain')}" style="text-decoration:none; padding:5px;border:1px solid #C0C0C0;">new species</a>
	        					</td>
	        				</tr>
	        		</table>
	        		<hr style="margins:5px;"/>
	        		<div class="span-9 border" style="padding-right:10px;">
	        			<p>
	        				Please select a file to upload. Your file should be in fasta format with headers such that each fasta header corresponds to the gene name you wish to import into the database. Please see this <a href="${resource(dir:'examples',file:'plastidDBExample.fasta')}">example</a> for further information.</li>
	        			</p>
	        			<br />
	        			<p>
	        				<label for="seqfile">File Name:</label>
	        				<input type="file" name="seqfile" />
        				</p>
        			</div>
        			<div class="span-7 last">
        			<p>
        				<p>Please indicate if you are uploading a series of genes for a species or a whole genome sequence for that species.</p><br /><br />
        				<label for="fileType">File Type:</label>
        				Gene Sequence <g:radio name="fileType" value="1" checked="true"/>
        				Genome <g:radio name="fileType" value="2"/>
        			</p>
        			</div>
	        	</fieldset>
	        	<g:submitButton  name="submit" value="Submit" />
            </g:uploadForm >
            <div class="commentCollection span-16 last" style="display:none;">
        	<div id="commentForm" >
        		<h4>Comments are listed below.  Please post the text you would use to describe the function of this page to your peers.  We want to describe the interface in such a way that it will make sense to biologists (and not just us computer scientists). If you have any questions please contact John at <a href="mailto:john@kerryhouse.net">john@kerryhouse.net</a></h4>
        		
        		<g:each var="comment" in="${comList}">
        			<div class="comment">${comment.value.body} -Posted By: ${comment.value.poster.username}</div>
        		</g:each>
        		
        		<g:form action="newComment">
        			<p>New Comment:</p>
        			<g:textArea name="viewName" value="index" class="hidden" />
        			<g:textArea name="controllerName" value="${this.controllerName}" class="hidden" />
        			<g:textArea name="commentText" class="span_6"/>
        			<br />
        			<g:submitButton name="newComment" value=" Add Comment" />
        		</g:form>
        	</div>
        	
        </div>
        </div>
        
    </body>
</html>
