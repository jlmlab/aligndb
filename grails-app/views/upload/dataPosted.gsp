 
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:if test ="${genome}">
        	<g:set var="entityName" value="${message(code: 'upload.label', default: 'Upload Genome file')}"/>
        </g:if>
        <g:else>
        	<g:set var="entityName" value="${message(code: 'upload.label', default: 'Upload Gene/Taxa file')}" />
        </g:else>
        <title><g:message code="default.show.label" args="[entityName]" /></title>
        <style type="text/css">
        .hidden { display: none; }
        </style>
        
    	<script type="text/javascript">
     		function hide( idName ){       
         		$('.showHide').addClass('hidden');
     		}
    	</script>
    </head>
        <content tag="menu-items">
    	</content>
    <body>
        <div class="body">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<div>
				<h2>Your data has been saved.</h2>
			</div>            
        </div>
        
    </body>
</html>
