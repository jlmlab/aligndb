
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title>User Management</title>

        <style type="text/css">
        .hidden 			{ 
        						display: none;
        					}
        .link 				{
        						cursor:pointer;
        					}
        .italic				{
        						font-style:italic;
        					}
        td.userRoleCheck 	{		
        						text-align: center;
        					}
        span.boldUnderlined	{	
        						text-decoration:underline;
        						font-weight: bold;
        					}
        tr.uPTR				{
        						border-bottom:1px solid #EEEEEE;
        						text-align:left;
        					}
        </style>
        
    	<script type="text/javascript">
     		function reveal( idName ){       
         		$('.sequenceDiv').addClass('seqHidden');
         		$('#'+idName).removeClass('seqHidden');    
    		}
    		
     		function hide( idName ){       
         		$('.showHide').addClass('hidden');
     		}
     		function populateDeleteBoxes(uid){
     			$.ajax( {url:'${createLink(action:"getUserPermissions")}',
 					dataType: 'json',
 					data: {UserID:uid},
 						success: function(data){
							$('#unameField').text(data.firstName + " " + data.lastName);
							$('#emailField').text(data.email);
							var permText = "";
							if(data.ROLE_USER){permText = permText + "General User, ";}
							if(data.ROLE_ADMIN){permText = permText + "Administrator, ";}
							if(data.ROLE_SUPERADMIN){permText = permText + "Super Administrator";}
							$('#permissionField').text(permText);
 	 					}
     			});
         		}
     		function populateRoleBoxes(uid){
     			$("input.userRoleCheck").attr('disabled',true);
     			$.ajax( {url:'${createLink(action:"getUserPermissions")}',
     					dataType: 'json',
     					data: {UserID:uid},
     					success: function(data){
         					if(!data.Locked){
         						$('#Unlock').attr('checked',true);
             				}
         					else{
         						$('#Unlock').attr('checked',false);
             				}
							if(data.ROLE_USER){
								$('#User').attr('checked',true);
							}
							else{
								$('#User').attr('checked',false);
							}
							if(data.ROLE_ADMIN){
								$('#Admin').attr('checked',true);
							}
							else{
								$('#Admin').attr('checked',false);
							}
							if(data.ROLE_SUPERADMIN){
								$('#SuperAdmin').attr('checked',true);
							}
							else{
								$('#SuperAdmin').attr('checked',false);
							}
         				}
         				}

     	     	);
     			$("input.userRoleCheck").removeAttr('disabled');

         	}

         	//document javascript binding
         	$(document).ready(function(){
         		var defUid = $('#nameSelector').val();
         		populateRoleBoxes(defUid);
				$('#nameSelector').change(function(){
						var uid = $('#nameSelector').val();
						populateRoleBoxes(uid);
					}
				);
				$('#delNameSelector').change(function(){
						var uid = $('#delNameSelector').val();
						populateDeleteBoxes(uid);
					
             	});
				var uid = $('#nameSelector').val();
				populateRoleBoxes(uid);
				var uid = $('#delNameSelector').val();
				populateDeleteBoxes(uid);
         	});
    	</script>  	
    	<style>
    		div.padded{
    			padding: 20px 30px 20px;
    		}
    	</style>	
    </head>
    
    <body>
        <div class="body">
            <h1>User Management</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="span-16 last" >	
        		<div class="padded" >
        		<g:form action="changeUserRoles">
        		<fieldset>
        		<legend>User Permissions</legend>
        		<div class="span-7 prepend-1"> 
        		<br/>
        		<span class="italic">User:</span>&nbsp
        		<select name="UserID" id="nameSelector">
        			<g:each in="${Users}">
        			<option value="${it.id}">${it.username}</option>
        			</g:each>
        		</select>
        		</div>
        		<div class="span-7 append-1 last">
        			<br/>
        			<span class="italic">Unlock User:</span>&nbsp<input type="checkbox" name="UNLOCK" value="true" class="userRoleCheck" id="Unlock">
        		</div>
        		<hr />
        		
        		<div class="span-14 prepend-1 append-1 last" align="left">     
        		<table class="span-10 prepend-2 append-2 last" id="userFlags">
        		<tr class="uPTR">
        			<td class="span-7"><span class="boldUnderlined">General User:</span><br /><br />
        				This user may view and download sequences in the database.</td>
        				<td class="userRoleCheck span-3 last"><input type="checkbox" name="USER" value="true" class="userRoleCheck" id="User"></td>
        		</tr>
        		<tr class="uPTR">
        			<td class="span-7"><span class="boldUnderlined">Administrator:</span><br /><br />
        				This user may upload sequences to the database and may change only those sequences which they have uploaded.</td>
        				<td class="userRoleCheck span-3 last"><input type="checkbox" name="ADMIN" value="true" class="userRoleCheck" id="Admin"></td>
        		</tr>
        		<tr class="uPTR">
        			<td class="span-7"><span class="boldUnderlined">Super Administrator:</span><br /><br />
        				This user may change user permissions. This user may change which sequence is considered most accurate for a gene/taxa combination and may change any sequence in the database.</td>
        			<td class="userRoleCheck span-3 last"><input type="checkbox" name="SUPERADMIN" value="true" class="userRoleCheck" id="SuperAdmin"></td>
        		</tr>
        		</table>
        		
        		
        		</div>
        		
        		<div class="span-15 last" align="center"> <input type="submit" value="Change Permissions"> </div>
        		
        		</div>
        		</fieldset>
        		</g:form>
      
        		
        	</div>
        
    </body>
</html>
