
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title>User Management</title>

        <style type="text/css">
        a					{
        						text-decoration: none;
        					}
        .hidden 			{ 
        						display: none;
        					}
        .link 				{
        						cursor:pointer;
        					}
        .italic				{
        						font-style:italic;
        					}
        td.userRoleCheck 	{		
        						text-align: center;
        					}
        span.boldUnderlined	{	
        						text-decoration:underline;
        						font-weight: bold;
        					}
        tr					{
        						text-align:left;
        					}
        td.submitRow		{
        						text-align:center;
        					}
        </style>
        
    	<script type="text/javascript">
     		
    	</script>  	
    		
    </head>
    
   	<content tag="menu-items">
		<sec:ifNotLoggedIn>
		</sec:ifNotLoggedIn>
		<sec:ifLoggedIn>
			<li><g:link action="create">New ${entityName}</g:link></li>
		</sec:ifLoggedIn>
	</content>

    <body>
        <div class="body">
            <h1>My Account</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <script type="text/javascript">
            
            </script>
            <div class="span-16 last" >	
        	<g:form action='myAccount' name='myAccountForm'>
        	<g:if test="${errCode == 1 }"><h4 class="notice">Please fill out both password fields below.</h4></g:if>
        	<g:if test="${errCode == 2 }"><h4 class="notice">The passwords entered do not match.</h4></g:if>
        	<g:if test="${errCode == 3 }"><h4 class="notice">Incorrect password.</h4></g:if>
			<table>
				<tr>
					<td>First Name:</td>
					<td>
						<g:textField name='firstName' size='40' value="${thisUser.firstName}"/>
					</td>
				</tr>
				<tr>
					<td>Last Name:</td>
					<td>
						<g:textField name='lastName'  size='40' labelCodeDefault='Lastname' value="${thisUser.lastName}"/>
					</td>
				</tr>
				<tr>
					<td>User Name:</td>
					<td>
						<g:textField name='username' labelCode='user.username.label' size='40' value="${thisUser.username}"/>
					</td>
				</tr>
				<tr>
					<td>e-mail:</td>
					<td>
						<g:textField name='email' value="${thisUser.email}" size='40' />
					</td>
				</tr>
				<tr>
					<td>Current Password:</td>
					<td>
						<g:passwordField name='password' size='40'/>
					</td>
				</tr>
				<tr>
					<td>Current Password (again):</td>
					<td>
						<g:passwordField name='password2' size='40'/>
					</td>
				</tr>
				<tr>
					<td id="submitRow"><g:submitButton name="Update" value="Submit Changes"/></td>
					<td>
						<a href="${createLink(action:'passwordChange')}">Change Password</a>
					</td>
				</tr>
			</table>
			<g:hiddenField name="changes" value="true" />
			</g:form>

        	</div>
        
    </body>
</html>
