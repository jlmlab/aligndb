

<%@ page import="edu.uga.aligndb.Gene" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'gene.label', default: 'Gene')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="body">
            <h1><g:message code="default.edit.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${geneInstance}">
            <div class="errors">
                <g:renderErrors bean="${geneInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${geneInstance?.id}" />
                <g:hiddenField name="version" value="${geneInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="name"><g:message code="gene.name.label" default="Name" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: geneInstance, field: 'name', 'errors')}">
                                    <g:textField name="name" value="${geneInstance?.name}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="sequences"><g:message code="gene.sequences.label" default="Sequences" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: geneInstance, field: 'sequences', 'errors')}">
                                    
<ul>
<g:each in="${geneInstance?.sequences?}" var="s">
    <li><g:link controller="sequence" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link></li>
</g:each>
</ul>
<g:link controller="sequence" action="create" params="['gene.id': geneInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'sequence.label', default: 'Sequence')])}</g:link>

                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
