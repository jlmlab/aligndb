
<%@ page import="edu.uga.aligndb.Gene" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'gene.label', default: 'Gene')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
        
        <style type="text/css">
        .hidden { display: none; }
        .link {cursor:pointer}
        </style>
        
    	<script type="text/javascript">
     		function hide( idName ){       
         		$('.showHide').addClass('hidden');
     		}
    	</script>       
        
    </head>
    <body>
            <h2><g:message code="default.list.label" args="[entityName]" /></h2>
            
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'gene.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="name" title="${message(code: 'gene.name.label', default: 'Name')}" />
                            
							<th><g:message code="gene.sequences.count" default="Total sequences" /></th>                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${geneInstanceList}" status="i" var="geneInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${geneInstance.id}">${fieldValue(bean: geneInstance, field: "id")}</g:link></td>
                        
                            <td><g:link action="show" id="${geneInstance.id}">${fieldValue(bean: geneInstance, field: "name")}</g:link></td>

							<td>${geneInstance.totalSequenceCount()} </td>                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${geneInstanceTotal}" />
            </div>
    </body>
</html>
