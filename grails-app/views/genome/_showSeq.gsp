<g:if test="${multiGen}">
	<g:if test="${genomeInstance.theOne}">
		<div id="${genomeInstance.id}" class="seqDiv">
	</g:if>
	<g:else>
		<div id="${genomeInstance.id}" class="hidden seqDiv">
	</g:else>
</g:if>
<g:else>
	<div id="${genomeInstance.id}" class="seqDiv">
</g:else>

            <div class="dialog">
                <table>
                    <tbody>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="genome.id.label" default="Id" /></td>
                            <td valign="top" class="value">${fieldValue(bean: genomeInstance, field: "id")}</td>
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="taxaInstance.name" default="Taxon Genome" /></td>
                            <td valign="top" class="value">${fieldValue(bean: taxaInstance, field: "name")}</td>
                        </tr>
                        
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="genomeInstance.state.label" default="State" /></td>
                            <td valign="top" class="value">${fieldValue(bean: genomeInstance, field: "state")}</td>
                        </tr>
                        
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="genomeInstance.theOne.label" default="The One" /></td>
                            <td valign="top" class="value"><g:formatBoolean boolean="${genomeInstance.theOne}" /></td>
                        </tr>
                        
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="genomeInstance.primaryUser.label" default="Primary User" /></td>
                            <td valign="top" class="value">${genomeInstance.primaryUser.username}</td>
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="genomeInstance.sequence" default="Sequence" /></td>
                            <td style="font-family:courier, courier new, Lucida Console,Monaco;"><g:seqRenderer size="65" seq="${genomeInstance.sequence}" /></td>
                        </tr>
                        
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="genomeInstance.dateCreated.label" default="Date Created" /></td>
                            <td valign="top" class="value"><g:formatDate date="${genomeInstance.dateCreated}" /></td>
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="genomeInstance.lastUpdated.label" default="Last Updated" /></td>
                            <td valign="top" class="value"><g:formatDate date="${genomeInstance?.lastUpdated}" /></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="buttons">
            	
            <g:if test="${ genomeInstance?.primaryUser.id == currentID}">
                <g:form>
                    <g:hiddenField name="id" value="${genomeInstance?.id}" />
                    <span class="button">
                 		<g:actionSubmit class="edit" action="edit" value="${message(code: 'default.button.edit.label', default: 'Edit')}" />
                    </span>
                    <span class="button">
                    	<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                    </span>
                </g:form>
            </g:if>
            </div>
</div>
