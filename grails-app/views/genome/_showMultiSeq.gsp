		<select name="sequenceSelection" id="sequenceSelector">
			<g:each in="${genomeCopies}">
					<option value="${it.id}" <%if(it.theOne){print "selected='true'"}%> >${it.primaryUser?.username} - ${it.lastUpdated}</option>
			</g:each>
		</select>
		<hr />
		<g:each in="${genomeCopies}" var="genomeInstance">
				<g:render template="showSeq" model="['multiGen':true,'genomeInstance':genomeInstance]" />
		</g:each>