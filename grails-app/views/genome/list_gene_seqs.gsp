<%@ page import="edu.uga.aligndb.Genome" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'genome.label', default: 'Genome')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
   	<content tag="menu-items">
		<sec:ifNotLoggedIn>
		</sec:ifNotLoggedIn>
		<sec:ifLoggedIn>
		</sec:ifLoggedIn>
	</content>

    <body>
        <div class="body">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="dialog">
                <table>
                    <tbody>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="genome.id.label" default="Id" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: genomeInstance, field: "id")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="genome.name.label" default="Genome" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: genomeInstance, field: "name")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="genome.sequences.label" default="Sequence Alignment" /></td>
                        </tr>  
                   </tbody>
                </table>
                
                <table>
                	<tbody>
                    	<g:each in="${genomeInstance.sequences}" var="s">
                        	<tr>
                            	<td><b>${s.gene.name}</b></td>
                                <td><g:seqRenderer size="65" seq="${s.sequence}" /></td>
                            </tr>	
                            
                           
                        </g:each>
                	</tbody>
                </table>
            </div>
            <div class="buttons">
                <g:form>
                    <g:hiddenField name="id" value="${genomeInstance?.id}" />
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'default.button.edit.label', default: 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </g:form>
            </div>
        </div>
    </body>
</html>
