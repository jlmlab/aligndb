<%@ page import="edu.uga.aligndb.Genome" %>
<html>
	<head>
		<meta http-equiv="Content-Type " content="text/html charset=UTF=8"/>
		<meta name="layout" content="main" />
		<g:set var="entityName" value="${message(code: 'genome.label', default: 'Genome')}" />
		<link rel="stylesheet" href="${resource(dir:'css',file:'Matrix/jquery.collapsibleCheckboxTree.css')}" />
        <link rel="stylesheet" href="${resource(dir:'css',file:'genome/colList.css')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
        <!-- Vici edit: description divs -->
        <style type="text/css">
        .hidden { display: none; }
        .link {cursor:pointer}
        </style>
        
        <script type="text/javascript">
     		function reveal( idName ){       
         		$('.sequenceDiv').addClass('seqHidden');
         		$('#'+idName).removeClass('seqHidden');    
    		}
    		
     		function hide( idName ){       
         		$('.showHide').addClass('hidden');
     		}
    	</script>
    	<!-- Vici edit: end description divs -->
    	
    	<g:javascript src="Matrix/jquery.collapsibleCheckboxTree.js"/>
    	
    	<script type="text/javascript">
        	$(document).ready(function(){
        		$('ul#test').collapsibleCheckboxTree({
    				checkParents : false, // When checking a box, all parents are checked
    				checkChildren : false, // When checking a box, all children are checked
    				uncheckChildren : true, // When unchecking a box, all children are unchecked
    				initialState : 'collapse' // Options - 'expand' (fully expanded), 'collapse' (fully collapsed) or default
    			});
        	});
        </script>  	
	</head>
	
	<body>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <div id="description" class="showHide">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean pharetra erat 
            turpis, et eleifend felis. Praesent tempor, dolor a eleifend vulputate, nisi nulla fermentum tortor, a vehicula purus 
            nisl placerat nibh. Sed interdum, est et mollis pellentesque, orci velit rutrum tellus, nec aliquam elit erat nec 
            enim. <a onclick="hide(description)" class="link">(hide description)</a>
            </div><br />
            

            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="span-16 last">
        		${retVal }
        	</div>
        
    </body>
</html>