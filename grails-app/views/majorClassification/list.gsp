
<%@ page import="edu.uga.aligndb.MajorClassification" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'majorClassification.label', default: 'MajorClassification')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
        <style type="text/css">
        .hidden { display: none; }
        </style>
        
    	<script type="text/javascript">
     		function hide( idName ){       
         		$('.showHide').addClass('hidden');
     		}
    	</script>   
    </head>
    <body>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <div id="description" class="showHide">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean pharetra erat 
            turpis, et eleifend felis. Praesent tempor, dolor a eleifend vulputate, nisi nulla fermentum tortor, a vehicula purus 
            nisl placerat nibh. Sed interdum, est et mollis pellentesque, orci velit rutrum tellus, nec aliquam elit erat nec 
            enim. <a onclick="hide(description)">(hide description)</a>
            </div><br />            
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'majorClassification.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="name" title="${message(code: 'majorClassification.name.label', default: 'Name')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${majorClassificationInstanceList}" status="i" var="majorClassificationInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${majorClassificationInstance.id}">${fieldValue(bean: majorClassificationInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: majorClassificationInstance, field: "name")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${majorClassificationInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
