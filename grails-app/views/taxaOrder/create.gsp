

<%@ page import="edu.uga.aligndb.TaxaOrder" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'SpeciesOrder.label', default: 'SpeciesOrder')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
   	<content tag="menu-items">
		<sec:ifNotLoggedIn>
		</sec:ifNotLoggedIn>
		<sec:ifLoggedIn>
		</sec:ifLoggedIn>
	</content>
    <body>

        <div class="body">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${taxaOrderInstance}">
            <div class="errors">
                <g:renderErrors bean="${taxaOrderInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="name"><g:message code="SpeciesOrder.name.label" default="Name" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: taxaOrderInstance, field: 'name', 'errors')}">
                                    <g:textField name="name" value="${taxaOrderInstance?.name}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="mc"><g:message code="SpeciesOrder.mc.label" default="Mc" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: taxaOrderInstance, field: 'mc', 'errors')}">
                                    <g:select name="mc.id" from="${edu.uga.aligndb.MajorClassification.list()}" optionKey="id" value="${taxaOrderInstance?.mc?.id}"  />
                                </td>
                                <td>
                                	<h5>-OR-</h5>
                                </td>
                                <td>
                                	<a href="${createLink(action:'create',controller:'majorClassification') }">Create New Major Classification</a>
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
