

<%@ page import="edu.uga.aligndb.TaxaOrder" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'SpeciesOrder.label', default: 'Order')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>

        <div class="body">
            <h1><g:message code="default.edit.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${taxaOrderInstance}">
            <div class="errors">
                <g:renderErrors bean="${taxaOrderInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${taxaOrderInstance?.id}" />
                <g:hiddenField name="version" value="${taxaOrderInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="name"><g:message code="SpeciesOrder.name.label" default="Name" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: taxaOrderInstance, field: 'name', 'errors')}">
                                    <g:textField name="name" value="${taxaOrderInstance?.name}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="families"><g:message code="SpeciesOrder.families.label" default="Families" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: taxaOrderInstance, field: 'families', 'errors')}">
                                    
								<ul>
									<g:each in="${taxaOrderInstance?.families?}" var="f">
    									<li><g:link controller="family" action="show" id="${f.id}">${f?.encodeAsHTML()}</g:link></li>
									</g:each>
								</ul>
								<g:link controller="family" action="create" params="['taxaOrder.id': taxaOrderInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'family.label', default: 'Family')])}</g:link>

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="mc"><g:message code="SpeciesOrder.mc.label" default="Mc" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: taxaOrderInstance, field: 'mc', 'errors')}">
                                    <g:select name="mc.id" from="${edu.uga.aligndb.MajorClassification.list()}" optionKey="id" value="${taxaOrderInstance?.mc?.id}"  />
                                </td>
                                <td>
                                	<h5>-OR-</h5>
                                </td>
                                <td>
                                	<a href="${createLink(action:'create',controller:'majorClassification') }">Create New Major Classification</a>
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
