
<%@ page import="edu.uga.aligndb.TaxaOrder" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'SpeciesOrder.label', default: 'SpeciesOrder')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'SpeciesOrder.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="name" title="${message(code: 'SpeciesOrder.name.label', default: 'Name')}" />
                        
                            <th><g:message code="SpeciesOrder.mc.label" default="Mc" /></th>
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${taxaOrderInstanceList}" status="i" var="taxaOrderInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${taxaOrderInstance.id}">${fieldValue(bean: taxaOrderInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: taxaOrderInstance, field: "name")}</td>
                        
                            <td>${fieldValue(bean: taxaOrderInstance, field: "mc")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${taxaOrderInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
