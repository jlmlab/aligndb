<ul id="test" style="font-size:small;">
	<g:each in="${majors}" var="major">
		<li>
			<input type='checkbox' name='${major.name}' class='majorClassificationSelect' />
			<label class="mcTag">Higher Taxon</label>: ${major.name}
			<ul>
				<g:each in="${major.orders}" var="order">
					<li>
						<input type='checkbox' name='${order.name}' class='orderSelect' />
						<label class="oTag">Order</label>: ${order.name}
						<ul>
							<g:each in="${order.families}" var="family">
								<li>
									<input type='checkbox' name='${family.name}' class='familySelect' />
									<label class="famTag">Family</label>: ${family.name}
									<ul>
										<g:each in="${family.taxa}" var="taxon">
											<li>
												<input type='checkbox' name='${taxon.name}' class='taxaSelect' />
												<label class="taxTag">Species</label>: ${taxon.name}
											</li>
										</g:each><!-- Taxa -->
									</ul>
								</li>
							</g:each><!-- families -->
						</ul>
					</li>
				</g:each><!-- orders -->
			</ul>
		</li>
	</g:each><!-- Majors -->
</ul>
