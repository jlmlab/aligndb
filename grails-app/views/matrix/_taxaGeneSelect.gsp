<div  class="span-9 border"  id="taxaList" style="text-align:left; border-color: #888888;">
	<h3>1: Select Species</h3>
	<p>Select parent elements to select all children species or expand nodes by clicking the + icons next to the classification names.</p>
	<br />
	<g:render template="taxaList" model="['majors':majors]"/>
</div>
<div class="span-1">
	&nbsp
</div>
<div  class="span-5 last" id="geneList" style="text-align:left;">
	<h3>2: Select Genes</h3>
	<p>Select as many genes as you like. hold down shift (or command for Mac users) to select multiple genes.</p>
	<br />
	<g:render template="geneList" model="['myGenes':myGenes]"/>
</div>
<div class="span-15 last" style="text-align:left;border-top:1px solid #888888">
	<h3>3: Generate Table</h3>
	<p>Click below to generate a table to review and refine which sequences you'd like to download.</p>
	<button onclick="submitToMap('${createLink(action:'detailedMap')}')">Generate Table</button>
</div>

