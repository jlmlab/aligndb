
<%@ page import="edu.uga.aligndb.Taxa" %>
<%@ page import="edu.uga.aligndb.Gene" %>
<%@ page import="edu.uga.aligndb.Sequence" %>

<html>
    <head>
        <!-- Meta Data -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'matrix.label', default: 'Species-gene matrix')}" />
        <title>
        	<g:message code="default.list.label" args="[entityName]" />
        </title>
        <!-- CSS stylesheets -->
        <link rel="stylesheet" href="${resource(dir:'css',file:'Matrix/jquery.collapsibleCheckboxTree.css')}" />
        <link rel="stylesheet" href="${resource(dir:'css',file:'Matrix/Matrix.css')}" />
        <link rel="stylesheet" href="${resource(dir:'css',file:'Matrix/pepper-grinder/jquery-ui-1.8.6.custom.css')}" />
        <link rel="stylesheet" href="${resource(dir:'css',file:'taxa/colList.css')}" />
        <!-- Javascript Includes -->
        <script src="http://cdn.jquerytools.org/1.2.3/jquery.tools.min.js"></script>
        
        <g:javascript src="Matrix/Matrix.js"/>
        
        <g:javascript src="Matrix/jquery.collapsibleCheckboxTree.js"/>
        
        <g:javascript src="jquery.escape.js"/>
        
        <g:javascript src="Matrix/jquery-ui-1.8.6.custom.min.js"/>
        
        <script type="text/javascript">
        	var tabs;
        	$(document).ready(function(){
        		tabs = $('#jqTabs').tabs({
            			select: function(event, ui){
							taxaMap = {};
							geneMap = {};
							if(!($(ui.tab).hasClass('descr'))){
								$(ui.panel).empty();
								$(ui.panel).append("<div style='text-align:center'><img src='${resource(dir:'images',file:'spinner.gif')}' /></div>");
							}
						},
						load: 	function(event, ui){
									var sIndex = tabs.tabs('option','selected');
									if(sIndex==2||sIndex==3){
										correctSelections();
										setList();
									}
								}
            		});
        		
            });
        	function correctSelections(){
        		var taxaList = new Array();
        		var geneList = new Array();
        		$('.highlighted').removeClass('highlighted');
    			for (var geneName in geneMap) {
    				if(geneMap[geneName]==0){
						$("."+$.escape(geneName)).removeClass('success');
					}
					else{
						$("."+$.escape(geneName)).addClass('success');
	        			geneList.push(geneName);
					}
					for(var taxaName in taxaMap){
    					if(taxaMap[taxaName]==0){
    						$("."+$.escape(taxaName)).removeClass('success');
    					}
    					else{
    						$("."+$.escape(taxaName)).addClass('success');
    	        			taxaList.push(taxaName);
    					}
    					if(geneMap[geneName]==1 && taxaMap[taxaName]==1){
    						$("td."+$.escape(taxaName) + "CM."+$.escape(geneName)+"CM").addClass("highlighted");
    						
    					}
    					
    				}
    			}
    			
    			
    	}        
        </script>
	</head>
    <body>
       <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <div id="description" class="showHide span-16 last">
            Use this page to view the status of the project or download sequences generated by species/gene combinations collected by clicking cells in either of the matrix views below. <a onclick="hide(description)" class="link">(hide description)</a>
            </div><br />
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div id="jqTabs" class="span-16 last"">
     			<ul>
     				<li><a href="#generalDescription" class="descr"><span>Information</span></a></li>
         			<li><a href="${createLink(action:'colorMap')}"><span>Full Map</span></a></li>
         			<li><a href="${createLink(action:'buildTaxaList')}" title="Specific Matrix"><span>Download Sequences</span></a></li>
         			<li><a href="${createLink(action:'genomeDownloadSelection')}" title="download_Genome"><span>Download Genomes</span></a></li>
     			</ul>
     			<div id="generalDescription" > 
     				<h4>Sequence viewing and downloading:</h4>
     				<ul>
     					<li><p>Use the tab <span style="font-weight:bold">Full Map</span> to view the status of completion of all of the possible species/gene pairs in the database. Hover over cells to get more information.</p></li>
     					<li><p>Use the tab <span style="font-weight:bold">Download Sequences</span> to generate a table to view the status of sequences of interest, verify your selection, and que them for e-mail to your registered address.</p></li>
     					<li><p>Use the tab <span style="font-weight:bold">Download Genomes</span> to que an email containing the stored genome sequences for any number of species of interest.</p></li>
     				</ul>
     			</div>
     			<div id="Specific_Matrix" style="overflow:auto;"></div>
     			<div id="download_Genome"></div>
			</div>
			<br />
			
        </div>
        
    </body>
</html>
