<div  class="span-9 border"  id="taxaList" style="text-align:left; border-color: #888888;">
	<h3>1: Select Species</h3>
	<p>Check parent elements to select all children species or expand nodes by clicking the + icons next to the classification names. Select any number of species and each genome will be e-mailed to you.</p>
	<br />
	<g:render template="taxaList" model="['majors':majors]"/>
</div>
<div class="span-1">
	&nbsp
</div>
<div  class="span-5 last" id="submitSection" style="text-align:left;">
	<h3>2: Submit</h3>
	<p>Click below to have the canonical genome(s) of each checked species e-mailed to the address listed in your account information.</p>
	<button onclick="dlGenome('${createLink(action:'selection')}')">Download</button>
</div>


