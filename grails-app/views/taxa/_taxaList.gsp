<ul id="test" style="font-size:small;">
	<g:each in="${majors}" var="major">
		<li>
			<a href='${createLink(controller:'majorClassification', action:'show',id:major.id)}'><label class="mcTag">Higher Taxon</label>: ${major.name}</a>
			<ul>
				<g:each in="${major.orders}" var="taxaOrder">
					<li>
						
						<a href='${createLink(controller:'taxaOrder', action:'show',id:taxaOrder.id)}'><label class="oTag">Order</label>: ${taxaOrder.name}</a>
						<ul>
							<g:each in="${taxaOrder.families}" var="family">
								<li>
									
									<a href='${createLink(controller:'family', action:'show',id:family.id)}'><label class="famTag">Family</label>: ${family.name}</a>
									<ul>
										<g:each in="${family.taxa}" var="taxon">
											<li>
												<a href='${createLink(action:'show',id:taxon.id)}'><label class="taxTag">Species</label>: ${taxon.name}</a>
												
											</li>
										</g:each><!-- Taxa -->
									</ul>
								</li>
							</g:each><!-- families -->
						</ul>
					</li>
				</g:each><!-- orders -->
			</ul>
		</li>
	</g:each><!-- Majors -->
</ul>
