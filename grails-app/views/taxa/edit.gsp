

<%@ page import="edu.uga.aligndb.Taxa" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'Species.label', default: 'Species')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
   	<content tag="menu-items">
		<sec:ifNotLoggedIn>
		</sec:ifNotLoggedIn>
		<sec:ifLoggedIn>
		</sec:ifLoggedIn>
	</content>

    <body>
        <div class="body">
            <h1><g:message code="default.edit.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${taxaInstance}">
            <div class="errors">
                <g:renderErrors bean="${taxaInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${taxaInstance?.id}" />
                <g:hiddenField name="version" value="${taxaInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="name"><g:message code="species.name.label" default="Name" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: taxaInstance, field: 'name', 'errors')}">
                                    <g:textField name="name" value="${taxaInstance?.name}" />
                                </td>
                            </tr>
                        	<tr class="prop">
                                <td valign="top" class="name">
                                    <label for="family"><g:message code="sequence.family.label" default="Family" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: taxaInstance, field: 'family', 'errors')}">
                                    <g:select name="family.id" from="${edu.uga.aligndb.Family.list()}" optionKey="id" value="${taxaInstance?.family?.id}"  />
                                </td>
                                <td>
                                	<h5>-OR-</h5>
                                </td>
                                <td>
                                	<a href="${createLink(action:'create',controller:'family') }">Create New Family</a>
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="sequences"><g:message code="species.sequences.label" default="Sequences" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: taxaInstance, field: 'sequences', 'errors')}">
                                    
<ul>
<g:each in="${taxaInstance?.sequences?}" var="s">
    <li><g:link controller="sequence" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link></li>
</g:each>
</ul>
<g:link controller="sequence" action="create" params="['taxa.id': taxaInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'sequence.label', default: 'Sequence')])}</g:link>

                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
