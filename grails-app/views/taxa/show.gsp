
<%@ page import="edu.uga.aligndb.Taxa" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'species.label', default: 'Species')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>


    <body>
        <div class="body">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="dialog">
                <table>
                    <tbody>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="species.id.label" default="Id" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: taxaInstance, field: "id")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="species.name.label" default="Name" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: taxaInstance, field: "name")}</td>
                            
                        </tr>

                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="family.name.label" default="Family" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: taxaInstance, field: "family")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="species.sequences.label" default="Sequences" /></td>
                            
                            <td valign="top" class="value">
                                <ul>
                                <g:each in="${finalList}" var="s">
                                    <li><g:link controller="sequence" action="show" params="[geneId:s.gene.id,taxaId:s.taxa.id]">${s?.gene.encodeAsHTML()}</g:link></li>
                                </g:each>
                                </ul>
                            </td>
                            
                        </tr>
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="species.genome.label" default="Genomes" /></td>
                            
                            <td valign="top"  class="value">
                                <ul>
                                <g:each in="${genList}" var="g">
                                    <li><g:link controller="genome" action="show" id="${g.taxa.id}">${g.primaryUser.username.encodeAsHTML()}, ${g?.dateCreated.encodeAsHTML()}</g:link></li>
                                </g:each>
                                </ul>
                            </td>
                            
                        </tr>
                    
                    </tbody>
                </table>
            </div>
            <sec:ifAnyGranted roles="ROLE_SUPERADMIN">
            <div class="buttons">
                <g:form>
                    <g:hiddenField name="id" value="${taxaInstance?.id}" />
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'default.button.edit.label', default: 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </g:form>
            </div>
            </sec:ifAnyGranted>
        </div>
    </body>
</html>
