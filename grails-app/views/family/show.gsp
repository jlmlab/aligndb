
<%@ page import="edu.uga.aligndb.Family" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'family.label', default: 'Family')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="body">
            <h1>Show Family</h1>
            
            <g:if test="${flash.message}">
            	<div class="message">apparently its active</div>
            </g:if>
            <div class="dialog">
                <table>
                    <tbody>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="family.id.label" default="Id" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: familyInstance, field: "id")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="family.name.label" default="Name" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: familyInstance, field: "name")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="family.order.label" default="Order" /></td>
                            
                            <td valign="top" class="value"><g:link controller="taxaOrder" action="show" id="${familyInstance?.order?.id}">${familyInstance?.order?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="family.taxa.label" default="Taxa" /></td>
                            
                            <td valign="top" style="text-align: left;" class="value">
                                <ul>
                                <g:each in="${familyInstance.taxa}" var="t">
                                    <li><g:link controller="taxa" action="show" id="${t.id}">${t?.encodeAsHTML()}</g:link></li>
                                </g:each>
                                </ul>
                            </td>
                            
                        </tr>
                    
                    </tbody>
                </table>
            </div>
            <sec:ifAnyGranted roles="ROLE_SUPERADMIN">
            <div class="buttons">
                <g:form>
                    <g:hiddenField name="id" value="${familyInstance?.id}" />
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'default.button.edit.label', default: 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </g:form>
            </div>
            </sec:ifAnyGranted>
        </div>
    </body>
</html>
