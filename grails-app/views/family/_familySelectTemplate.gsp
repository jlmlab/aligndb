						<div class="span-14 prepend-1 append-1  last"  id="Families">
						
						<div class="span-14 prepend-1 append-1  last">
							<table>
							<tr>
								<g:if test="${!createNew}">
									<td>Select a Family</td>
								</g:if>
								<td>
									<g:if test="${!createNew}">Or</g:if> create a new Family.
								</td>
							</tr>
	        				<tr>
	        					
	        						<g:if test="${!createNew}">
	        						<td>
	        							<select id="famSelect"  name="famName" onclick="empty_Text('famText')">
	        								<option value="null">
	        									Nothing Selected
	        								</option>
	        								<g:each var="family" in="${fList}">
	        									<option value="${family.id}">
	        										${family.name}
	        									</option>
	        								</g:each>
	        							</select>
	        							</td>
	        						</g:if>
	        					
	 							<td>
	        						<g:textField name="famNameFinal" id="famText" onclick="deselect('famSelect')"/>
	        					</td>
	        					</tr>
	        			</table>
	        			</div>
	        				<div class="span-16 last" style="text-align:center" id="famSelected">
	        					<button onclick='backToTOSelect()' class="prepend-1 span-2">Back</button>
	        					<div class="span-10">&nbsp</div>
	        					<button onclick="getTaxaList()" class="span-2 append-1 last">Next</button>
	        				</div>
	        			</div>