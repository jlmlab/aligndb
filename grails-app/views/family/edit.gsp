

<%@ page import="edu.uga.aligndb.Family" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'family.label', default: 'Family')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="body">
            <h1><g:message code="default.edit.label" args="Family" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${familyInstance}">
            <div class="errors">
                <g:renderErrors bean="${familyInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${familyInstance?.id}" />
                <g:hiddenField name="version" value="${familyInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="name"><g:message code="family.name.label" default="Name" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: familyInstance, field: 'name', 'errors')}">
                                    <g:textField name="name" value="${familyInstance?.name}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="order"><g:message code="family.order.label" default="Order" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: familyInstance, field: 'order', 'errors')}">
                                    <g:select name="order.id" from="${edu.uga.aligndb.TaxaOrder.list()}" optionKey="id" value="${familyInstance?.order?.id}"  />
                                </td>
                                <td>
                                	<h5>-OR-</h5>
                                </td>
                                <td>
                                	<a href="${createLink(action:'create',controller:'taxaOrder') }">Create New Order</a>
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="taxa"><g:message code="family.taxa.label" default="Taxa" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: familyInstance, field: 'taxa', 'errors')}">
                                    
<ul>
<g:each in="${familyInstance?.taxa?}" var="t">
    <li><g:link controller="taxa" action="show" id="${t.id}">${t?.encodeAsHTML()}</g:link></li>
</g:each>
</ul>
<g:link controller="taxa" action="create" params="['family.id': familyInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'taxa.label', default: 'Taxa')])}</g:link>

                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
