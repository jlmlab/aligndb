
<%@ page import="edu.uga.aligndb.Family" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'family.label', default: 'Family')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="body">
            <h1><g:message code="default.list.label" args="Families" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'family.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="name" title="${message(code: 'family.name.label', default: 'Name')}" />
                        
                            <th><g:message code="family.order.label" default="Order" /></th>
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${familyInstanceList}" status="i" var="familyInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${familyInstance.id}">${fieldValue(bean: familyInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: familyInstance, field: "name")}</td>
                        
                            <td>${fieldValue(bean: familyInstance, field: "order")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${familyInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
