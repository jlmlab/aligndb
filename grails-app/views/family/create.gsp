

<%@ page import="edu.uga.aligndb.Family" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'family.label', default: 'Family')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="body">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${familyInstance}">
            <div class="errors">
                <g:renderErrors bean="${familyInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="name"><g:message code="family.name.label" default="Name" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: familyInstance, field: 'name', 'errors')}">
                                    <g:textField name="name" value="${familyInstance?.name}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="order"><g:message code="family.order.label" default="Order" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: familyInstance, field: 'order', 'errors')}">
                                    <g:select name="order.id" from="${edu.uga.aligndb.TaxaOrder.list()}" optionKey="id" value="${familyInstance?.order?.id}"  />
                                </td>
                                <td>
                                	<h5>-OR-</h5>
                                </td>
                                <td>
                                	<a href="${createLink(action:'create',controller:'taxaOrder') }">Create New Order</a>
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
