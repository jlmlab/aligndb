import edu.uga.aligndb.*;

class BootStrap {

    def springSecurityService
	    def init = { servletContext ->
		DataSourceUtils.tune(servletContext)
	    def adminRole = new Role(authority: 'ROLE_ADMIN').save(flush: true)
	    def superAdminRole = new Role(authority: 'ROLE_SUPERADMIN').save(flush: true)
	    def userRole = new Role(authority: 'ROLE_USER').save(flush: true)
	
	    String password = springSecurityService.encodePassword('readme')
		String password2 = springSecurityService.encodePassword('readme')
	    def testUser2 = new User(username: 'jkerry', enabled: true, password: password2, lastName:'kerry', firstName:'john', email:'john@uga.edu')
		def testUser = new User(username: 'raj', enabled: true, password: password, lastName:'Ayyampalayam', firstName:'Raj', email:'raj@plantbio.uga.edu')
		testUser.save(flush: true)
		testUser2.save(flush: true)
	
	    UserRole.create testUser, adminRole, true
		UserRole.create testUser, userRole, true
		UserRole.create testUser, superAdminRole, true
		
		
		
		def mc = MajorClassification.findByName('Not Classified') 
		mc = mc ? mc : new MajorClassification(name:'Not Classified')
		mc.save(flush:true)
		
		def order = TaxaOrder.findByName('Not Classified') 
		order = order ? order : new TaxaOrder(name: 'Not Classified', mc:mc)
		order.save(flush:true)

		def family = Family.findByName('Not Classified') 
		family = family ? family : new Family(name: 'Not Classified',order:order)
		family.save(flush:true)
		
    }
    def destroy = {
    
    }
}
