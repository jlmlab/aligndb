dataSource {
    pooled = true

	driverClassName = "com.mysql.jdbc.Driver"
	username = "raj"
	password = "mysqlsparc"

}
hibernate {
    cache.use_second_level_cache = true
    cache.use_query_cache = true
    cache.provider_class = 'org.hibernate.cache.EhCacheProvider'
}
// environment specific settings
environments {
    development {
        dataSource {
            dbCreate = "update" // one of 'create', 'create-drop','update'
    			url = "jdbc:mysql://jnsdb.plantbio.uga.edu/aligndbDev"

        }
    }
    test {
        dataSource {
            dbCreate = "update"
            url = "jdbc:hsqldb:mem:testDb"
        }
    }
    production {
        dataSource {
            dbCreate = "update"
            url = "jdbc:mysql://jnsdb.plantbio.uga.edu/aligndb"
        }
    }
}
