package edu.uga.aligndb

import grails.plugins.springsecurity.Secured;
@Secured(['ROLE_USER'])
class GeneController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [geneInstanceList: Gene.list(params), geneInstanceTotal: Gene.count()]
    }

    @Secured(['ROLE_SUPERADMIN'])
    def create = {
        def geneInstance = new Gene()
        geneInstance.properties = params
		
        return [geneInstance: geneInstance]
    }

    @Secured(['ROLE_ADMIN'])
    def save = {
        def geneInstance = new Gene(params)
        if (geneInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'gene.label', default: 'Gene'), geneInstance.id])}"
            redirect(action: "show", id: geneInstance.id)
        }
        else {
            render(view: "create", model: [geneInstance: geneInstance])
        }
    }

    def show = {
        def geneInstance = Gene.get(params.id)
        if (!geneInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'gene.label', default: 'Gene'), params.id])}"
            redirect(action: "list")
        }
        else {
			def finalList = [:];
			geneInstance.sequences.each{value->
				if(!finalList.getAt(value.taxa.id)){
					finalList.putAt(value.taxa.id,value)
				}
			}
			def fList = []
			finalList.each{key,value->
				fList.add(value);
			}
			fList.sort{it.taxa.name}
			
            [geneInstance: geneInstance,finalList:fList]
        }
    }

    @Secured(['ROLE_ADMIN'])
    def edit = {
        def geneInstance = Gene.get(params.id)
        if (!geneInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'gene.label', default: 'Gene'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [geneInstance: geneInstance]
        }
    }

    @Secured(['ROLE_ADMIN'])
    def update = {
        def geneInstance = Gene.get(params.id)
        if (geneInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (geneInstance.version > version) {
                    
                    geneInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'gene.label', default: 'Gene')] as Object[], "Another user has updated this Gene while you were editing")
                    render(view: "edit", model: [geneInstance: geneInstance])
                    return
                }
            }
            geneInstance.properties = params
            if (!geneInstance.hasErrors() && geneInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'gene.label', default: 'Gene'), geneInstance.id])}"
                redirect(action: "show", id: geneInstance.id)
            }
            else {
                render(view: "edit", model: [geneInstance: geneInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'gene.label', default: 'Gene'), params.id])}"
            redirect(action: "list")
        }
    }

    @Secured(['ROLE_SUPERADMIN'])
    def delete = {
        def geneInstance = Gene.get(params.id)
        if (geneInstance) {
            try {
                geneInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'gene.label', default: 'Gene'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'gene.label', default: 'Gene'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'gene.label', default: 'Gene'), params.id])}"
            redirect(action: "list")
        }
    }
}
