package edu.uga.aligndb
import grails.plugins.springsecurity.Secured;
@Secured(['ROLE_USER'])
class FamilyController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
		
		//if this is the list method which requires a form output for uploader
		if(params.form){
			//if the user is requesting to create a new taxa
			if(params.newTO == "true"){
				//if the taxa order is a new create, then validate that the taxa name does not exist.
				if(TaxaOrder.findByName(params.tOName)){
					render "<p>Error: Duplicate tOName</p>"
					return;
				}
				//else render the next menu with only a create new dialog.
				else{
					render(template:'familySelectTemplate',model: [createNew:true])
					return;
				}
			}
			//else render the full family list of the selected taxaorder
			render(template:'familySelectTemplate',model: [fList:TaxaOrder.get(params.tOId).families])
		}
		//else render standard list behavior
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [familyInstanceList: Family.list(params), familyInstanceTotal: Family.count()]
    }

	@Secured(['ROLE_ADMIN'])
    def create = {
		//redirected to the structured create on the major classification to taxa tree.
        //def familyInstance = new Family()
        //familyInstance.properties = params
		redirect(controller:"upload",action:"newTaxaChain")
		//return [familyInstance: familyInstance]
    }

@Secured(['ROLE_ADMIN'])
    def save = {
        def familyInstance = new Family(params)
        if (familyInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'family.label', default: 'Family'), familyInstance.id])}"
            redirect(action: "show", id: familyInstance.id)
        }
        else {
            render(view: "create", model: [familyInstance: familyInstance])
        }
    }

    def show = {
        def familyInstance = Family.get(params.id)
        if (!familyInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'family.label', default: 'Family'), params.id])}"
            redirect(action: "list")
        }
        else {
            [familyInstance: familyInstance]
        }
    }

@Secured(['ROLE_ADMIN'])
    def edit = {
        def familyInstance = Family.get(params.id)
        if (!familyInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'family.label', default: 'Family'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [familyInstance: familyInstance]
        }
    }

@Secured(['ROLE_ADMIN'])
    def update = {
        def familyInstance = Family.get(params.id)
        if (familyInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (familyInstance.version > version) {
                    
                    familyInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'family.label', default: 'Family')] as Object[], "Another user has updated this Family while you were editing")
                    render(view: "edit", model: [familyInstance: familyInstance])
                    return
                }
            }
            familyInstance.properties = params
            if (!familyInstance.hasErrors() && familyInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'family.label', default: 'Family'), familyInstance.id])}"
                redirect(action: "show", id: familyInstance.id)
            }
            else {
                render(view: "edit", model: [familyInstance: familyInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'family.label', default: 'Family'), params.id])}"
            redirect(action: "list")
        }
    }

	@Secured(['ROLE_SUPERADMIN'])
    def delete = {
        def familyInstance = Family.get(params.id)
        if (familyInstance) {
            try {
                familyInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'family.label', default: 'Family'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'family.label', default: 'Family'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'family.label', default: 'Family'), params.id])}"
            redirect(action: "list")
        }
    }
}
