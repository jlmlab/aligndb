package edu.uga.aligndb
import java.util.Map;

import grails.plugins.springsecurity.Secured;
import static groovy.io.FileType.*
import org.codehaus.groovy.grails.web.json.*;
import grails.converters.JSON;

@Secured(['ROLE_ADMIN'])
class UploadController {

	def uploadService
	def springSecurityService
	
	
	def newTaxaChain = {

		if(params.batchData){
			def mClass;
			if(params.newMC != "true"){
				mClass = MajorClassification.get(params.mcId);
			}
			else{
				mClass = new MajorClassification(name:params.mcName)
				mClass.save(flush:true);
			}
			def taxOrd;
			if(params.newTO != "true"){
				taxOrd = TaxaOrder.get(params.tOId);
			}
			else{
				taxOrd = new TaxaOrder(name:params.tOName,mc:mClass)
				taxOrd.save(flush:true);
			}
			def fam;
			if(params.newFam != "true"){
				fam = Family.get(Integer.parseInt(params.famId));
			}
			else{
				fam = new Family(name:params.famName,order:taxOrd)
				fam.save(flush:true);
			}
			
			def taxa = new Taxa(name:params.taxaName, family:fam);

			if(!taxa.save()){
				taxa.errors.each {
					println it
				}
			}
			
			render("Successfully Added Items")
		}
		else{
			def mcList = MajorClassification.listOrderByName()
		
			return [mcList:mcList]
		}
		
	}
	
	
	def index = { 
		def v = ViewComments.find("from ViewComments where controllerName='${this.controllerName}' and viewName='index'")
		def comList = [:]
		v.comments.each {
			comList["${it.id}"]=it
		}
		def taxa = Taxa.listOrderByName(params)
		return [taxa:taxa, comList:comList]
			
	}

	def setCommentHoldersInDB = {
		def CD = new ViewComments(controllerName:"${this.controllerName}",viewName:'index')
		CD.save()
		def CC = new ViewComments(controllerName:"${this.controllerName}",viewName:'preprocess')
		CC.save(flush: true)
		
		render 'success'
	}
	
	def newComment ={
		
		def v = ViewComments.find("from ViewComments where controllerName='${params.controllerName}' and viewName='${params.viewName}'")
		def userDetails = springSecurityService.principal
		def thisUser = User.get(userDetails.id)
		
		v.addComment(thisUser,params.commentText)
		redirect(controller:params.controllerName,action:'index')
		
		
	}
	
	def checkAmbiguity = {
		def index = 0;
		def geneList = [];
		Gene.listOrderByName().each{geneList.add(index, it.toString()); index++;}   //retrieves gene list from DB
		def geneExists = uploadService.geneCheck(geneList,params.geneName);
		render(contentType:"text/html",text:"${geneExists}")
	}
	
	def corrections = {
			def filename = params.seqFile;
			def downloadedfile = request.getFile('seqfile');
			
			try{
				def webRootDir = servletContext.getRealPath("/")
				def userDetails = springSecurityService.principal
				def loggedUser = User.get(userDetails.id)
				def userDir = new File(webRootDir, "/upload/${loggedUser.username}")
				userDir.mkdirs()
				downloadedfile.transferTo(new File(userDir,downloadedfile.originalFilename));
				def biofile = "${webRootDir}/upload/${loggedUser.username}/${downloadedfile.originalFilename}"
				def thisFile = new File(biofile);
				//System.out.println params;
				
				thisFile.eachLine{ line ->
					int ind = 0;
					def original;
					def neo;
					def pair = line.split(", ")
					
					pair.each{
						if(ind==0){
							neo = it;
						}
						else{
							original = it;
						}
						ind++
					}
					def indTaxa = Taxa.findByName("${original}")
					
					if(indTaxa){
						indTaxa.save(flush:true);
					}
				}
			}
			catch(Exception ex){
				flash.message = ex.getMessage();
				redirect(action:"index");
			}
	}
	
	def upload = {
			def fileName = params.seqfile;
			String titleName = params.titleName;
			def downloadedfile = request.getFile('seqfile');

			if(downloadedfile.isEmpty() || titleName.equals("")){
				flash.message = "Error: Please input a title and select a file.";
				redirect(action:"create");
			}
			else{

				try{
					def webRootDir = servletContext.getRealPath("/")
					def userDetails = springSecurityService.principal
					def loggedUser = User.get(userDetails.id)
					def userDir = new File(webRootDir, "/upload/${loggedUser.username}")
					userDir.mkdirs()
					downloadedfile.transferTo(new File(userDir,downloadedfile.originalFilename));  
					def biofile = "${webRootDir}/upload/${loggedUser.username}/${downloadedfile.originalFilename}"
					//System.out.println params;

					if(params.uploadType.equals("1")){

						boolean loaded = uploadService.loadGene(titleName,biofile,params.overWrite);
						if(loaded){	   		
							flash.message = "Upload successful"
							return [titleName : titleName]   
						}
						else{
							redirect(action:"create");
						}
					}       
					if(params.uploadType.equals("2")){
						boolean loaded = uploadService.loadTaxon(titleName,biofile,params.overWrite);
						if(loaded){	   		
							flash.message = "Upload successful"
							return [titleName : titleName]   
						}
						else{
							redirect(action:"create");
						}
					}
				}
				catch(Exception ex){
					flash.message = ex.getMessage();
					redirect(action:"index");
				}
			}
	}
	
	//returns JSONArray back to view
	def preprocess = {
		def v = ViewComments.find("from ViewComments where controllerName='${this.controllerName}' and viewName='preprocess'")
		def comList = [:]
		v.comments.each {
			comList["${it.id}"]=it
		}
		def fileName = params.seqfile;
		String titleName = params.titleName;
		def downloadedfile = request.getFile('seqfile');

		if(downloadedfile.isEmpty() || titleName.equals("")){
			flash.message = "Error: Please input a title and select a file.";
			redirect(action:"create");
		}
		else{

			try{
				def webRootDir = servletContext.getRealPath("/")
				def userDetails = springSecurityService.principal
				def loggedUser = User.get(userDetails.id)
				def userDir = new File(webRootDir, "/upload/${loggedUser.username}")
				userDir.mkdirs()
				downloadedfile.transferTo(new File(userDir,downloadedfile.originalFilename));
				def biofile = "${webRootDir}upload/${loggedUser.username}/${downloadedfile.originalFilename}"
				
				//System.out.println(params.fileType);
				if(params.fileType.equals("1")){
					Map seqList = uploadService.uploadTaxon(titleName,biofile);
					
					if(!seqList.isEmpty()){
						return [seqList : seqList, comList:comList]
					}
					else{
						def err = [error:'Error in upload'] 
						return [err:err, comList:comList]
					}
				}
				else if(params.fileType.equals("2")){
					Map genomeObj = uploadService.uploadGenome(titleName,biofile);
				
					if(!genomeObj.isEmpty()){
						return [genomeObj : genomeObj, comList:comList]
						redirect(action:'dataPosted')
					}
					else{
						def err = [error:'Error in upload']
						return [err:err, comList:comList]
					}
				}
			}
			catch(Exception ex){
				def err = [error:'Error in upload'] 
				return [err:err, comList:comList];
			}
		}
	}
	
	def dataPosted = {
		if(params.jsonText){
			def geneName
			def seqString
			boolean added
			String jsonText  = params.jsonText
			boolean overWrite = /*params.overWrite*/ true
			def mapList = new JSONObject(jsonText);
			def taxon = mapList.get("taxaName")
			mapList.each {key,seqMap -> 
				if(key != 'taxaName'){
					geneName = seqMap['geneName']
					seqString = seqMap['seqString']
					added = uploadService.uploadSingleGene(taxon,geneName,seqString,overWrite)
					if(added == false)
						render 'failed'
				}
			}
		}
		else if(params.genomeObj){
 				
		}
		return {mapList:mapList}
	}
	
	
	def bulkupload = {
		def webRootDir = servletContext.getRealPath("/")
		def filePath = "${webRootDir}/${params.location}"
		def uploadType = params.type
		
		def myDir = new File(filePath);
		
		myDir.eachFile FILES, {File geneFile ->
			String name = geneFile.name
			name = name.replaceAll("\\..*", "")
			log.error "${name}  ${geneFile.name}"
			if (uploadType == "gene"){
				uploadService.loadGene(name,"${filePath}/${geneFile.name}",false)
			}else if(uploadType == "taxa"){
				uploadService.loadTaxon(name,"${filePath}/${geneFile.name}",false)
			}
		}
		render(view:'upload')
	}
	
}
