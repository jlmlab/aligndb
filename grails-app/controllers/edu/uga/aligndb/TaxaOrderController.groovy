package edu.uga.aligndb
import grails.plugins.springsecurity.Secured;
import grails.converters.*

@Secured(['ROLE_USER'])
class TaxaOrderController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }
	
    def list = {
		if(params.form){
			if(params.newMC == "true"){
				
				if(MajorClassification.findByName(params.mcName)){
					render "<p>Error: Duplicate mcName</p>"
					return;
				}
				else{
					render(template:'taxaOrderSelectTemplate',model: [createNew:true])
					return;
				}
			}
			render(template:'taxaOrderSelectTemplate',model: [tOList:MajorClassification.get(params.mcId).orders,createNew:false])
		}
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [taxaOrderInstanceList: TaxaOrder.list(params), taxaOrderInstanceTotal: TaxaOrder.count()]
    }

@Secured(['ROLE_ADMIN'])
    def create = {
		//redirected to the structured create on the major classification to taxa tree.
		redirect(controller:"upload",action:"newTaxaChain")
        //def taxaOrderInstance = new TaxaOrder()
        //taxaOrderInstance.properties = params
        //return [taxaOrderInstance: taxaOrderInstance]
    }

@Secured(['ROLE_ADMIN'])
    def save = {
        def taxaOrderInstance = new TaxaOrder(params)
        if (taxaOrderInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'taxaOrder.label', default: 'TaxaOrder'), taxaOrderInstance.id])}"
            redirect(action: "show", id: taxaOrderInstance.id)
        }
        else {
            render(view: "create", model: [taxaOrderInstance: taxaOrderInstance])
        }
    }

    def show = {
        def taxaOrderInstance = TaxaOrder.get(params.id)
        if (!taxaOrderInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'taxaOrder.label', default: 'TaxaOrder'), params.id])}"
            redirect(action: "list")
        }
        else {
            [taxaOrderInstance: taxaOrderInstance]
        }
    }

@Secured(['ROLE_ADMIN'])
    def edit = {
        def taxaOrderInstance = TaxaOrder.get(params.id)
        if (!taxaOrderInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'taxaOrder.label', default: 'TaxaOrder'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [taxaOrderInstance: taxaOrderInstance]
        }
    }

@Secured(['ROLE_ADMIN'])
    def update = {
        def taxaOrderInstance = TaxaOrder.get(params.id)
        if (taxaOrderInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (taxaOrderInstance.version > version) {
                    
                    taxaOrderInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'taxaOrder.label', default: 'TaxaOrder')] as Object[], "Another user has updated this TaxaOrder while you were editing")
                    render(view: "edit", model: [taxaOrderInstance: taxaOrderInstance])
                    return
                }
            }
            taxaOrderInstance.properties = params
            if (!taxaOrderInstance.hasErrors() && taxaOrderInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'taxaOrder.label', default: 'TaxaOrder'), taxaOrderInstance.id])}"
                redirect(action: "show", id: taxaOrderInstance.id)
            }
            else {
                render(view: "edit", model: [taxaOrderInstance: taxaOrderInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'taxaOrder.label', default: 'TaxaOrder'), params.id])}"
            redirect(action: "list")
        }
    }

@Secured(['ROLE_SUPERADMIN'])
    def delete = {
        def taxaOrderInstance = TaxaOrder.get(params.id)
        if (taxaOrderInstance) {
            try {
                taxaOrderInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'taxaOrder.label', default: 'TaxaOrder'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'taxaOrder.label', default: 'TaxaOrder'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'taxaOrder.label', default: 'TaxaOrder'), params.id])}"
            redirect(action: "list")
        }
    }
}
