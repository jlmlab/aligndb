package edu.uga.aligndb
import grails.plugins.springsecurity.Secured;
import grails.converters.JSON;
import org.codehaus.groovy.grails.plugins.springsecurity.NullSaltSource



class UserController extends grails.plugins.springsecurity.ui.UserController {
	
	def springSecurityService
	final int ROLE_ADMIN_ID = 1;
	final int ROLE_SUPERADMIN_ID = 2;
	final int ROLE_USER_ID = 3;
	
	def passwordChange = {
		def userDetails = springSecurityService.principal
		def thisUser = User.get(userDetails.id)
		if(params.changes){
		if(params.password && params.password2){
			if(params.password == params.password2){
				String salt = saltSource instanceof NullSaltSource ? null : thisUser.username
				String password = springSecurityService.encodePassword(params.password, salt)
				if(thisUser.password == password){
					
					if(params.npassword && params.npassword2){
						if(params.npassword == params.npassword2){
							String nsalt = saltSource instanceof NullSaltSource ? null : thisUser.username
							String npassword = springSecurityService.encodePassword(params.npassword, salt)
							thisUser.password = npassword;
						}
						else{
							return [thisUser:thisUser,errCode:5]
						}
					}
					else{
						return [thisUser:thisUser,errCode:4]
					}
					
				}
				else{
					return [thisUser:thisUser,errCode:3]
				}
			}
			else{
				//passwords dont match
				return [thisUser:thisUser,errCode:2]
			}
		}
		else{
			//please fill out both
			return [thisUser:thisUser,errCode:1]
		}
		springSecurityService.reauthenticate thisUser.username
		redirect (action:myAccount)
		}
		return
	}
	
	def myAccount = {
		
		def userDetails = springSecurityService.principal
		def thisUser = User.get(userDetails.id)
		if(params.changes){
		if(params.password && params.password2){
			if(params.password == params.password2){
				String salt = saltSource instanceof NullSaltSource ? null : thisUser.username
				String password = springSecurityService.encodePassword(params.password, salt)
				if(thisUser.password == password){
					if(params.firstName){
						thisUser.firstName = params.firstName
					}
					if(params.lastName){
						thisUser.lastName = params.lastName
					}
					if(params.username){
						thisUser.username = params.username
					}
					if(params.email){
						thisUser.email = params.email
					}
				}
				else{
					return [thisUser:thisUser,errCode:3]
				}
			}
			else{
				//passwords dont match
				return [thisUser:thisUser,errCode:2]
			}
		}
		else{
			//please fill out both
			return [thisUser:thisUser,errCode:1]
		}
		springSecurityService.reauthenticate thisUser.username
		return [thisUser:thisUser,errCode:0]
		}
		return [thisUser:thisUser,errCode:0]
	}
	
	@Secured(['ROLE_SUPERADMIN'])
	def management = {
		
		def taxaL = Taxa.listOrderByName();
		def Users = User.listOrderByUsername();
		
		return [taxaL:taxaL,Users:Users]
		
	}
	@Secured(['ROLE_SUPERADMIN'])
	def getUserPermissions ={
		def items = [:]
		if(params.UserID==null){
			render items as JSON
		}
		else{
			def uid = params.UserID.toInteger()
			def myUser = User.get(uid)
			items['firstName']=myUser.firstName
			items['lastName']=myUser.lastName
			items['email']=myUser.email
			items['Locked']=myUser.accountLocked
			items['ROLE_USER']=UserRole.get(uid,ROLE_USER_ID)
			items['ROLE_ADMIN']=UserRole.get(uid,ROLE_ADMIN_ID)
			items['ROLE_SUPERADMIN']=UserRole.get(uid,ROLE_SUPERADMIN_ID)
		}
		render items as JSON
	}

	def changeUserRoles = {
		
		def uid = params.UserID.toInteger();
		def Unlock = params.UNLOCK;
		def userRole = params.USER;
		def adminRole = params.ADMIN;
		def sAdminRole = params.SUPERADMIN;
		
		if(Unlock=="true"){
			User.get(uid).accountLocked=false;
		}
		else{
			User.get(uid).accountLocked=true;
		}
		
		//create user role for user with id uid
		if(userRole=="true"){
			//does the role already exist?
			if(UserRole.get(uid,ROLE_USER_ID)){
				//end(Forward)
			}
			//if not then create it:
			else{
				UserRole.create(User.get(uid), Role.get(ROLE_USER_ID));
			}
		}
		else{
			//does the role exist if so delete it
			if(UserRole.get(uid,ROLE_USER_ID)){
				UserRole.remove(User.get(uid), Role.get(ROLE_USER_ID));
			}
			//else ignore delete request
			else{
				//end(Forward)
			}
		}
		
		//create admin role for user with id uid
		if(adminRole=="true"){
			//does the role already exist?
			if(UserRole.get(uid,ROLE_ADMIN_ID)){
				//end(Forward)
			}
			//if not then create it:
			else{
				UserRole.create(User.get(uid), Role.get(ROLE_ADMIN_ID));
			}
		}
		else{
			//does the role exist if so delete it
			if(UserRole.get(uid,ROLE_ADMIN_ID)){
				UserRole.remove(User.get(uid), Role.get(ROLE_ADMIN_ID));
			}
			//else ignore delete request
			else{
				//end(Forward)
			}
		}
		//create super admin role for user with id uid
		if(sAdminRole=="true"){
			//does the role already exist?
			if(UserRole.get(uid,ROLE_SUPERADMIN_ID)){
				//end(Forward)
			}
			//if not then create it:
			else{
				UserRole.create(User.get(uid), Role.get(ROLE_SUPERADMIN_ID));
			}
		}
		else{
			//does the role exist if so delete it
			if(UserRole.get(uid,ROLE_SUPERADMIN_ID)){
				UserRole.remove(User.get(uid), Role.get(ROLE_SUPERADMIN_ID));
			}
			//else ignore delete request
			else{
				//end(Forward)
			}
		}
		
		redirect(action:'management')
	}
}
