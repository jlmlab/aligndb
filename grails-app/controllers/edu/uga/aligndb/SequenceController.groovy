package edu.uga.aligndb

import grails.plugins.springsecurity.Secured;
import org.codehaus.groovy.grails.plugins.springsecurity.SpringSecurityUtils
@Secured(['ROLE_USER'])
class SequenceController {

	def springSecurityService
	def springSecurityUtils = new SpringSecurityUtils();
	def retrievalService
	def userDetails
	def primaryUser
	
	final int ROLE_ADMIN_ID = 1;
	final int ROLE_SUPERADMIN_ID = 2;
	final int ROLE_USER_ID = 3;
	
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

	
    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [sequenceInstanceList: Sequence.list(params), sequenceInstanceTotal: Sequence.count()]
    }

    @Secured(['ROLE_SUPERADMIN'])
    def create = {
        def sequenceInstance = new Sequence()
        sequenceInstance.properties = params 
        return [sequenceInstance: sequenceInstance]
    }

    @Secured(['ROLE_ADMIN'])
    def save = {
		userDetails = springSecurityService.principal
		primaryUser = User.get(userDetails.id)
		params.put "primaryUser", primaryUser
        def sequenceInstance = new Sequence(params)
		
        if (sequenceInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'sequence.label', default: 'Sequence'), sequenceInstance.id])}"
            redirect(action: "show", id: sequenceInstance.id)
        }
        else {
            render(view: "create", model: [sequenceInstance: sequenceInstance])
        }
    }

	@Secured(['ROLE_USER'])
    def show = {
		userDetails = springSecurityService.principal
		primaryUser = User.get(userDetails.id)
		if(params.id){
		def sequenceInstance = Sequence.get(params.id)
		if (!sequenceInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'sequence.label', default: 'Sequence'), params.id])}"
            redirect(action: "list")
        }
        else {
            [sequenceInstance: sequenceInstance, currentID: userDetails.id, multi:false]
        }
		}
		else if(params.taxaId && params.geneId){
			def sequences = retrievalService.get(params.taxaId.toInteger(),params.geneId.toInteger());
			sequences.sort{it.lastUpdated}

			return [sequences:sequences, currentID: userDetails.id, multi:true]
		}
		else{
			redirect(action: "list")
		}
    }

	@Secured(['ROLE_ADMIN'])
    def edit = {
		
        def sequenceInstance = Sequence.get(params.id)
        if (!sequenceInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'sequence.label', default: 'Sequence'), params.id])}"
            redirect(action: "list")
        }
        else {
			userDetails = springSecurityService.principal
			primaryUser = User.get(userDetails.id)
			if(sequenceInstance.primaryUser == primaryUser || springSecurityUtils.ifAnyGranted("ROLE_SUPERADMIN")){
				return [sequenceInstance: sequenceInstance]
			}
			else{
				flash.message = "You are not authorized to edit this sequence. Please contact an administrator to request a change or report this message if you think it to be in error."
				redirect(action:'list');
			}
        }
    }

    @Secured(['ROLE_ADMIN'])
    def update = {
        def sequenceInstance = Sequence.get(params.id)
        if (sequenceInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (sequenceInstance.version > version) {
                    
                    sequenceInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'sequence.label', default: 'Sequence')] as Object[], "Another user has updated this Sequence while you were editing")
                    render(view: "edit", model: [sequenceInstance: sequenceInstance])
                    return
                }
            }
			userDetails = springSecurityService.principal
			primaryUser = User.get(userDetails.id)
			if(sequenceInstance.primaryUser == primaryUser || springSecurityUtils.ifAnyGranted("ROLE_SUPERADMIN")){
            sequenceInstance.properties = params
            	if (!sequenceInstance.hasErrors() && sequenceInstance.save(flush: true)) {
                	flash.message = "${message(code: 'default.updated.message', args: [message(code: 'sequence.label', default: 'Sequence'), sequenceInstance.id])}"
                	redirect(action: "show", id: sequenceInstance.id)
            	}
				else {
					render(view: "edit", model: [sequenceInstance: sequenceInstance])
            	}
			}
			else{
				flash.message = "You are not authorized to update this sequence. Please contact an administrator to request a change or report this message if you think it to be in error."
				redirect(action:'list')
			}
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'sequence.label', default: 'Sequence'), params.id])}"
            redirect(action: "list")
        }
    }

    @Secured(['ROLE_ADMIN'])
    def delete = {
		userDetails = springSecurityService.principal
		primaryUser = User.get(userDetails.id)
        def sequenceInstance = Sequence.get(params.id)
        if (sequenceInstance) {
            try {
				if(sequenceInstance.primaryUser.id == primaryUser.id || UserRole.get(primaryUser.id,ROLE_SUPERADMIN_ID)){
					sequenceInstance.delete(flush: true)
					flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'sequence.label', default: 'Sequence'), params.id])}"
					redirect(action: "list")
				}
				else{
					flash.message = "${message(code: 'Sequence was not deleted. Must be owner of sequence or super administator. Please contact super administator.')}"
					redirect(action: "list")
				}
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'sequence.label', default: 'Sequence'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'sequence.label', default: 'Sequence'), params.id])}"
            redirect(action: "list")
        }
    }
}
