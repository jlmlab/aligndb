package edu.uga.aligndb

import grails.plugins.springsecurity.Secured;
import org.codehaus.groovy.grails.plugins.springsecurity.SpringSecurityUtils

@Secured(['ROLE_USER'])
class GenomeController {
	
	def springSecurityService
	def springSecurityUtils = new SpringSecurityUtils();
	def retrievalService
	def userDetails
	def primaryUser
	
	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
    def index = {
		redirect(action: "list", params: params)
	}
	
	def list = {
		def majors = MajorClassification.listOrderByName()
		
		StringBuffer retVal = new StringBuffer("")
		retVal << "<ul id=\"test\">"
		retVal << "<br />"
		def Fam = 'family'
		def Ord = 'taxaOrder'
		def MC = 'majorClassification'
		majors.each { major ->
				
				retVal << "<li>\n"
				retVal << "Higher Taxon: ${major.name}\n"
				
				retVal << "<ul>\n"
				major.orders.each {taxaOrder ->
					
					retVal << "\t<li>\n"
					retVal << "Order: ${taxaOrder.name}\n"
					retVal << "\t<ul>\n"
					taxaOrder.families.each {family ->
						
						retVal << "\t\t<li>"
						retVal << "Family: ${family.name}\n"
						
						retVal << "\t\t<ul>\n"
						family.taxa.each {taxon->
							
							retVal << "\t\t\t<li>"
							retVal << "\t\t\t<name='${taxon.name}' class='taxaSelect' />\n"
							retVal << "\t\t\t<a href='${createLink(action:'show',id:taxon.id)}'>Genome: ${taxon.name}</a>\n"
							retVal << "\t\t\t</li>\n"
						} 
						retVal << "\t\t</ul>\n"
						retVal << "\t\t</li>\n"
					}
					retVal << "</ul>\n"
					retVal << "</li>\n"
				}
				retVal << "</ul>\n"
				retVal << "</li>\n"
		}
		retVal << "</ul>\n"

		[genomeInstanceList: Genome.list(params), genomeInstanceTotal: Genome.count(),retVal:retVal]
	}
	
	@Secured(['ROLE_SUPERADMIN'])
	def create = {
		def genomeInstance = new Genome()
		genomeInstance.properties = params
		return [genomeInstance: genomeInstance]
	}
	
	@Secured(['ROLE_ADMIN'])
	def save = {
		userDetails = springSecurityService.principal
		primaryUser = User.get(userDetails.id)
		params.put "primaryUser", primaryUser
		def genomeInstance = new Genome(params)
		
		if (genomeInstance.save(flush: true)) {
			flash.message = "${message(code: 'default.created.message', args: [message(code: 'genome.label', default: 'Genome'), genomeInstance.id])}"
			redirect(action: "show", id: genomeInstance.id)
		}
		else {
			render(view: "create", model: [genomeInstance: genomeInstance])
		}
	}
	
	@Secured(['ROLE_ADMIN'])
	def edit = {
		def genomeInstance = Genome.get(params.id)
		if (!genomeInstance) {
			flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'genome.label', default: 'Genome'), params.id])}"
			redirect(action: "list")
		}
		else {
			userDetails = springSecurityService.principal
			primaryUser = User.get(userDetails.id)
			if(genomeInstance.primaryUser == primaryUser || springSecurityUtils.ifAnyGranted("ROLE_SUPERADMIN")){
				return [genomeInstance: genomeInstance]
			}
			else{
				flash.message = "You are not authorized to edit this genome. Please contact an administrator to request a change or report this message if you think it to be in error."
				redirect(action:'list');
			}
		}
	}
	
	def show = {
		def genomeInstance
		userDetails = springSecurityService.principal
		primaryUser = User.get(userDetails.id)
		def taxaInstance = Taxa.get(params.id) 
		def genomeCopies = Genome.findAll("from Genome as genomeInstance where genomeInstance.taxa=?",[taxaInstance])
		def numOfCopies = genomeCopies.size()
		if(params.id){
			genomeInstance = Genome.findByTaxa(taxaInstance)
			if (!genomeInstance) {
			    flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'sequence.label', default: 'Sequence'), params.id])}"
			    redirect(action: "list")
			}
			else{
				if(numOfCopies == 1){
					[genomeInstance: genomeInstance, taxaInstance: taxaInstance, currentID: userDetails.id, multi:false]
				}
				else if(numOfCopies > 1){
					genomeCopies.sort{it.lastUpdated}
					
					return [genomeCopies: genomeCopies, taxaInstance: taxaInstance, currentID: userDetails.id, multi: true]
				}
			}
		}
		else{
			redirect(action:list)	
		}
    }

	@Secured(['ROLE_ADMIN'])
	def update = {
		def genomeInstance = Genome.get(params.id)
		if (genomeInstance) {
			if (params.version) {
				def version = params.version.toLong()
				if (genomeInstance.version > version) {
					
					genomeInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'sequence.label', default: 'Sequence')] as Object[], "Another user has updated this Sequence while you were editing")
					render(view: "edit", model: [genomeInstance: sequenceInstance])
					return
				}
			}
			userDetails = springSecurityService.principal
			primaryUser = User.get(userDetails.id)
			if(genomeInstance.primaryUser == primaryUser || springSecurityUtils.ifAnyGranted("ROLE_SUPERADMIN")){
				genomeInstance.properties = params
				if (!genomeInstance.hasErrors() && genomeInstance.save(flush: true)) {
					flash.message = "${message(code: 'default.updated.message', args: [message(code: 'genome.label', default: 'Genome'), genomeInstance.id])}"
					redirect(action: "show", id: genomeInstance.id)
				}
				else {
					render(view: "edit", model: [genomeInstance: genomeInstance])
				}
			}
			else{
				flash.message = "You are not authorized to update this genome. Please contact an administrator to request a change or report this message if you think it to be in error."
				redirect(action:'list')
			}
		}
		else {
			flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'sequence.label', default: 'Sequence'), params.id])}"
			redirect(action: "list")
		}
	}

	@Secured(['ROLE_SUPERADMIN'])
	def delete = {
		def genomeInstance = Genome.get(params.id)
		if (genomeInstance) {
			try {
				genomeInstance.delete(flush: true)
				flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'genomeInstance.label', default: 'Genome'), params.id])}"
				redirect(action: "list")
			}
			catch (org.springframework.dao.DataIntegrityViolationException e) {
				flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'genomeInstance.label', default: 'Genome'), params.id])}"
				redirect(action: "show", id: params.id)
			}
		}
		else {
			flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'genomeInstance.label', default: 'Genome'), params.id])}"
			redirect(action: "list")
		}
	}
}
