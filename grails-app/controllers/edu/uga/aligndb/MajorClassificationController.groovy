package edu.uga.aligndb
import grails.plugins.springsecurity.Secured;
@Secured(['ROLE_USER'])
class MajorClassificationController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
		if(params.form){
			render(template:'MajorClassificationSelectTemplate',model: [mcList:MajorClassification.listOrderByName()])
		}
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [majorClassificationInstanceList: MajorClassification.list(params), majorClassificationInstanceTotal: MajorClassification.count()]
    }

@Secured(['ROLE_ADMIN'])
    def create = {
		//redirected to the structured create on the major classification to taxa tree.
        //def majorClassificationInstance = new MajorClassification()
        //majorClassificationInstance.properties = params
		redirect(controller:"upload",action:"newTaxaChain")
        //return [majorClassificationInstance: majorClassificationInstance]
    }

@Secured(['ROLE_ADMIN'])
    def save = {
        def majorClassificationInstance = new MajorClassification(params)
        if (majorClassificationInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'majorClassification.label', default: 'MajorClassification'), majorClassificationInstance.id])}"
            redirect(action: "show", id: majorClassificationInstance.id)
        }
        else {
            render(view: "create", model: [majorClassificationInstance: majorClassificationInstance])
        }
    }

    def show = {
        def majorClassificationInstance = MajorClassification.get(params.id)
        if (!majorClassificationInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'majorClassification.label', default: 'MajorClassification'), params.id])}"
            redirect(action: "list")
        }
        else {
            [majorClassificationInstance: majorClassificationInstance]
        }
    }

@Secured(['ROLE_ADMIN'])
    def edit = {
        def majorClassificationInstance = MajorClassification.get(params.id)
        if (!majorClassificationInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'majorClassification.label', default: 'MajorClassification'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [majorClassificationInstance: majorClassificationInstance]
        }
    }

@Secured(['ROLE_ADMIN'])
    def update = {
        def majorClassificationInstance = MajorClassification.get(params.id)
        if (majorClassificationInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (majorClassificationInstance.version > version) {
                    
                    majorClassificationInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'majorClassification.label', default: 'MajorClassification')] as Object[], "Another user has updated this MajorClassification while you were editing")
                    render(view: "edit", model: [majorClassificationInstance: majorClassificationInstance])
                    return
                }
            }
            majorClassificationInstance.properties = params
            if (!majorClassificationInstance.hasErrors() && majorClassificationInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'majorClassification.label', default: 'MajorClassification'), majorClassificationInstance.id])}"
                redirect(action: "show", id: majorClassificationInstance.id)
            }
            else {
                render(view: "edit", model: [majorClassificationInstance: majorClassificationInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'majorClassification.label', default: 'MajorClassification'), params.id])}"
            redirect(action: "list")
        }
    }

@Secured(['ROLE_SUPERADMIN'])
    def delete = {
        def majorClassificationInstance = MajorClassification.get(params.id)
        if (majorClassificationInstance) {
            try {
                majorClassificationInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'majorClassification.label', default: 'MajorClassification'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'majorClassification.label', default: 'MajorClassification'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'majorClassification.label', default: 'MajorClassification'), params.id])}"
            redirect(action: "list")
        }
    }
}
