package edu.uga.aligndb
import java.util.ArrayList;
import org.grails.comments.*;
import grails.plugins.springsecurity.Secured;

@Secured(['ROLE_USER'])
class MatrixController{
	def downloadService
	def springSecurityService
	
	@Secured(['ROLE_ADMIN'])
	def setCommentHoldersInDB = {
		def CC = new ViewComments(controllerName:"${this.controllerName}",viewName:'index')
		CC.save()
		
		redirect(controller:'upload',action:'runOnce')
	}
	
    def index = {
		def v = ViewComments.find("from ViewComments where controllerName='${this.controllerName}' and viewName='index'")
		def comList = [:]
		v.comments.each {
			comList["${it.id}"]=it
		}
		def genes = Gene.listOrderByName();
		def taxa = Taxa.listOrderByName();
		return [comList:comList,genes:genes,taxa:taxa]
	}
	
	def newComment ={
		
		def v = ViewComments.find("from ViewComments where controllerName='${params.controllerName}' and viewName='${params.viewName}'")
		def userDetails = springSecurityService.principal
		def thisUser = User.get(userDetails.id)
		
		v.addComment(thisUser,params.commentText)
		redirect(controller:params.controllerName,action:params.viewName)
		
		
	}
	
	def buildGeneList = {
		
		def genes = Gene.listOrderByName(params)
		StringBuffer retVal = new StringBuffer("");
		
		retVal << "<select name='genes' id='geneList' size='7' multiple='multiple'>"
		genes.each {gene -> 
			retVal << "<option value='${gene.id}' class='geneSelect'>${gene.name}</option>"
			}
		
		retVal << "</select>"
		
		render(text:retVal.toString(),contentType:'text/html')
	}
	
	def buildTaxaList = {
		
		def majors = MajorClassification.listOrderByName(params)
		def myGenes = Gene.listOrderByName(params)
		render(template:"taxaGeneSelect",model:[majors:majors,myGenes:myGenes])
	}
	
	def genomeDownloadSelection = {
		def majors = MajorClassification.listOrderByName(params)
		render(template:"genomeDlSelect",model:[majors:majors])
	}	
	
	def detailedMap = {
		def genesList = params.genes.split(',').collect {it}
		def taxaList = params.taxa.split(',').collect {it}
		ArrayList<Long> ints = new ArrayList<Long>();
		genesList.each {geneId ->
			ints.add (new Long(geneId.toInteger()));
		}
		List taxa = Taxa.findAllByNameInList(taxaList)
		List genes = Gene.findAllByIdInList(ints)
		
		def c = Sequence.createCriteria()
		def sequences = c.list {
			and{
				order('taxa','desc')
				order('gene','desc')
			}
		}
		def seqMap = [:]
				
		sequences.each {seq ->
				if (!seqMap[seq.taxa.name]){
					seqMap[seq.taxa.name] = [:]
				}
				def tmpMap = seqMap.get(seq.taxa.name)
				tmpMap."${seq.gene.name}" = seq.whatIsTheState()
				seqMap."${seq.taxa.name}" = tmpMap
				
		}
		StringBuffer retVal = new StringBuffer()
		retVal << '<hr />'
		retVal << '<table><tr>'
		retVal << '<td><b>Selected Species:</b></td>'
		retVal << '<td><select id="selTaxa"></select></td>'
		retVal << '<td><button onclick="getAndSelectTaxa()">Remove this species</button></td>'
		retVal << '</tr><tr><td><b>Selected Genes:</b></td>'
		retVal << '<td><select id="selGene"></select></td>'
		retVal << '<td><button onclick="getAndSelectGene()">Remove this gene</button></td>'
		retVal << '</tr>'
		retVal << '<tr><hr /><td>&nbsp</td><td>'
		retVal << '<select id="outputType"><option value="fasta">Fasta</option>'
		retVal << '<option value="afasta">Aligned Fasta</option><option value="nexus">Nexus File</option></select></td>'
		retVal << "<td><button onclick=\"sendMail('${createLink(action:'selection')}')\">E-mail these sequences</button></td>"
		retVal << '</tr></table>'
		retVal << '<hr />'
		retVal << "<div><p style=\"font-size: medium;\">The letters 'C'&nbsp;'I'&nbsp;and&nbsp;'N' stand for 'Complete Sequence', 'In-complete Sequence' and 'Not applicable'.</p></div>"
		retVal << "<table id=\"globalMatrix\" class=\"span-1\">\n" 
		retVal << 	"<thead>\n" 
		retVal << "\t\t<tr>\n" 
		retVal << "\t\t\t<td>taxa/gene</td>"
		for(gene in genes){
			//onclick=\"selectGene('${gene.name}')\" removed
			retVal << "\t\t\t<td  class=\"${gene.name}\">\n"
			retVal << "\t\t\t\t${fieldValue(bean: gene, field: 'name')}\n"										                        		
			retVal << "\t\t\t</td>"
		}
		retVal << "\t\t</tr>\n"
		retVal << "\t</thead>\n"
		
		retVal << "<tbody>"
		for(taxaInstance in taxa){ 
			retVal << "\t<tr>\n"
			//onclick=\"selectTaxa('${taxaInstance.name}')\" removed
			retVal << "<td  class=\"${taxaInstance.name}\">${fieldValue(bean: taxaInstance, field: 'name')}</td>"
				for(gene in genes){
				def seqStat = seqMap.get(taxaInstance.name)?.get(gene.name)
				if (seqStat == null){
					seqStat = 'N'
				}
				//onclick=\"colorMapSelect('${taxaInstance.name}','${gene.name}')\" removed
				retVal << "\t\t<td class=\"${gene.name}CM ${taxaInstance.name}CM colorMapCell\"  title=\"Species: ${taxaInstance.name}, Gene: ${gene.name}\" >"
				retVal << "${seqStat}"
				retVal << "</td>\n"
			}
			retVal << "\t</tr>\n"
		}
		retVal << "</tbody>\n</table>"
		render(text:retVal.toString(),contentType:'text/html')
	}
    

	
	def colorMap = {
		def genes = Gene.listOrderByName(params)
		def taxa = Taxa.listOrderByName(params)
		def sequences = Sequence.list()
		def seqMap = [:]
		
		sequences.each {seq ->
			if (!seqMap[seq.taxa.name]){
				seqMap[seq.taxa.name] = [:]
			}
			def tmpMap = seqMap.get(seq.taxa.name)
			tmpMap."${seq.gene.name}" = seq.whatIsTheState()
			seqMap."${seq.taxa.name}" = tmpMap
			
		}
				StringBuffer retVal = new StringBuffer()
				retVal << "<div class='span-16 last'><h5>Hover over a cell to see what gene and species is represented.</h5></div>"
		retVal << "<table style=\"width: ${6*genes.size()};\" id=\"colorMap\">\n"
		for(taxaInstance in taxa){
			retVal << "\t<tr>\n"
			for(gene in genes){
				def seqStat = seqMap.get(taxaInstance.name)?.get(gene.name)
				if (seqStat == null){
					seqStat = 'N'
				}
				def seqClass
				if(seqStat.compareTo('C')==0){
					seqClass="lightBlue"
				}
				else if(seqStat.compareTo('I')==0){
					seqClass="lightRed"
				}
				else{
					seqClass="black"
				}
				//onclick=\"colorMapSelect('${taxaInstance.name}','${gene.name}')\" removed temporarily.
				retVal << "\t\t<td class=\"${gene.name}CM ${taxaInstance.name}CM colorMapCell ${seqClass}\"  title=\"Species: ${taxaInstance.name}, Gene: ${gene.name}\" >"
				retVal << "</td>\n"
			}
			retVal << "\t</tr>\n"
		}
		retVal << "</table>"
		render(text:retVal.toString(),contentType:'text/html')
		
	}


    
    def selection = {
		def userDetails = springSecurityService.principal
		def thisUser = User.get(userDetails.id)
		if(!params.genome){
			def genes = params.genes.split(',').collect {it}
			def taxa = params.taxa.split(',').collect {it}
			def outputType = params.outputType
		
			List taxaList = Taxa.findAllByNameInList(taxa)
			List geneList = Gene.findAllByNameInList(genes)	
			if (taxaList.isEmpty()){
				taxaList = Taxa.listOrderByName()
			}
			if (geneList.isEmpty()){
				geneList = Gene.listOrderByName()
			}
			def webRootDir = servletContext.getRealPath("/")
			downloadService.mailGeneSequences(webRootDir, thisUser.email, outputType, taxaList, geneList)
			render("Mail sent.")
		}
		else{

			def taxa = params.taxa.split(',').collect {it}
			
			def outputType = params.outputType
			List taxaList = Taxa.findAllByNameInList(taxa)
			def webRootDir = servletContext.getRealPath("/")
			downloadService.mailGenomeSequences(webRootDir, thisUser.email, outputType, taxaList)
			render("Mail Sent")
		}
	}
    
    
}
