package edu.uga.aligndb
import groovy.text.SimpleTemplateEngine

import org.codehaus.groovy.grails.commons.ApplicationHolder as AH
import org.codehaus.groovy.grails.plugins.springsecurity.NullSaltSource
import org.codehaus.groovy.grails.plugins.springsecurity.SpringSecurityUtils
import org.codehaus.groovy.grails.plugins.springsecurity.ui.RegistrationCode
import org.codehaus.groovy.grails.plugins.springsecurity.ui.RegistrationCode

class RegisterController extends grails.plugins.springsecurity.ui.RegisterController {
	def retrievalService
	final int ROLE_SUPERADMIN_ID = 2;
	def index = {
		[command: new RegisterCommand(),details:"Please enter some details concerning your institution affiliation and/or enter comments for the administrators confirming your application."]
	}
	
	def register = { RegisterCommand command ->
		if (command.hasErrors()) {
			render view: 'index', model: [command: command,details:params.details]
			return
		}

		String salt = saltSource instanceof NullSaltSource ? null : command.username
		String password = springSecurityService.encodePassword(command.password, salt)
		def user = lookupUserClass().newInstance(email: command.email, username: command.username,
				password: password, accountLocked: true, enabled: true, lastName: command.lastName, firstName: command.FirstName)
		if (!user.validate() || !user.save()) {
			// TODO
		}

		def registrationCode = new RegistrationCode(username: user.username).save()
		String url = generateLink('verifyRegistration', [t: registrationCode.token])

		def conf = SpringSecurityUtils.securityConfig
		
		def body = conf.ui.register.emailBody
		if (body.contains('$')) {
			body = evaluate(body, [user: user, url: url])
		}
		mailService.sendMail {
			to command.email
			from conf.ui.register.emailFrom
			subject conf.ui.register.emailSubject
			html body.toString()
		}
		def sAdminList = retrievalService.getUsersWithRole(ROLE_SUPERADMIN_ID)
		def msg = "<p>A new user has applied for the MonATol database with the following information:<br/>Username:    ${user.username}<br/>Name:    ${user.firstName} ${user.lastName}<br />E-Mail:    ${user.email}<br /> And provided the following registration comments: <br />${params.details}<br /><br />Please log into PlastidDB, confirm this user, and grant permissions appropriate for their access level.</p>"
		sAdminList.each{
			String email = it.email;
			mailService.sendMail {
				to email
				from conf.ui.register.emailFrom
				subject "New user registration for the MonATol Plastid Gene Database"
				html msg
			}
		}

		render view: 'index', model: [emailSent: true]
	
	}
	
}

class RegisterCommand extends grails.plugins.springsecurity.ui.RegisterCommand {
	String lastName
	String FirstName
	
} 