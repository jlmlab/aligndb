package edu.uga.aligndb

import grails.plugins.springsecurity.Secured;

@Secured(['ROLE_USER'])
class TaxaController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

	def verifyExistance = {
		if(params.form){
				if(Family.findByName(params.famName)){
					render "<p>Error: Duplicate famName</p>"
					return;
				}
				render(template:'taxaSelectTemplate', model:[:])
			
		}
		
		
	}
	
    def list = {
        
		def majors = MajorClassification.listOrderByName()
        [majors:majors]
    }

    @Secured(['ROLE_ADMIN'])
    def create = {
		//redirected to the structured create on the major classification to taxa tree.
		redirect(controller:"upload",action:"newTaxaChain")
        //def taxaInstance = new Taxa()
        //taxaInstance.properties = params
        //return [taxaInstance: taxaInstance]
    }

    @Secured(['ROLE_ADMIN'])
    def save = {
        def taxaInstance = new Taxa(params)
        if (taxaInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'taxa.label', default: 'Taxa'), taxaInstance.id])}"
            redirect(action: "show", id: taxaInstance.id)
        }
        else {
            render(view: "create", model: [taxaInstance: taxaInstance])
        }
    }

    def show = {
        def taxaInstance = Taxa.get(params.id)
        if (!taxaInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'taxa.label', default: 'Taxa'), params.id])}"
            redirect(action: "list")
        }
        else {
			
			def finalList = [:];
			taxaInstance.sequences.each{value->
				if(!finalList.getAt(value.gene.id)){
					finalList.putAt(value.gene.id,value)
				}
			}
			def genList = [:];
			taxaInstance.genome.each{geno->
				genList.putAt(geno.id,geno);
			}
			def fList = []
			finalList.each{key,value->
				fList.add(value);
			}
			def gList = []
			genList.each{key,val->
				gList.add(val);
			}
			gList.sort{it.dateCreated};
			fList.sort{it.gene.name}
			
            [taxaInstance: taxaInstance, finalList:fList,genList:gList]
        }
    }

    @Secured(['ROLE_SUPERADMIN'])
    def edit = {
        def taxaInstance = Taxa.get(params.id)
        if (!taxaInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'taxa.label', default: 'Taxa'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [taxaInstance: taxaInstance]
        }
    }

    @Secured(['ROLE_ADMIN'])
    def update = {
        def taxaInstance = Taxa.get(params.id)
        if (taxaInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (taxaInstance.version > version) {
                    
                    taxaInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'taxa.label', default: 'Taxa')] as Object[], "Another user has updated this Taxa while you were editing")
                    render(view: "edit", model: [taxaInstance: taxaInstance])
                    return
                }
            }
            taxaInstance.properties = params
            if (!taxaInstance.hasErrors() && taxaInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'taxa.label', default: 'Taxa'), taxaInstance.id])}"
                redirect(action: "show", id: taxaInstance.id)
            }
            else {
                render(view: "edit", model: [taxaInstance: taxaInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'taxa.label', default: 'Taxa'), params.id])}"
            redirect(action: "list")
        }
    }

    @Secured(['ROLE_SUPERADMIN'])
    def delete = {
        def taxaInstance = Taxa.get(params.id)
        if (taxaInstance) {
            try {
                taxaInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'taxa.label', default: 'Taxa'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'taxa.label', default: 'Taxa'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'taxa.label', default: 'Taxa'), params.id])}"
            redirect(action: "list")
        }
    }
}
