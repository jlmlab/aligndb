class SeqdisplayTagLib {
	def seqRenderer = { attrs, body ->
		def size = attrs.size.toInteger()
		def seq = attrs.seq.replace("*","")
		def seqLength = seq.size()
		def loops = seqLength.intdiv(size)
		def j = 0
		def i = 0
		loops.times{
			j = i + size -1
			out << "${seq.getAt(i..j)}<br />"
			i = j + 1
		}
		if (i<seqLength-1){
			out << "${seq.getAt(i..seqLength-1)}<br />"
		}
	}

}
