package aligndb
import edu.uga.aligndb.*;


class FindSeqCodeTagLib {
	def renderSeqCode ={attrs, body ->
		Gene gene = attrs.gene
		Taxa taxa = attrs.taxa
		Sequence seq = Sequence.findByGeneAndTaxa(gene, taxa)
		if (seq != null){
			if (seq.state == "Complete"){
				out << "C"
			}else{
				out << "I"
			}
		}else{
			out << "N"
		}
	}
}
