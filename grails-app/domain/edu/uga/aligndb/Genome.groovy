package edu.uga.aligndb
import org.grails.comments.*;
import java.util.Date;

class Genome implements Commentable{
	
	String sequence
	String state
	Boolean theOne = false
	Date dateCreated
	Date lastUpdated
	User primaryUser
	
	static belongsTo = [taxa:Taxa]
	
	static constraints = {
		state(inList:['Incomplete','Complete'])
		sequence(blank:false,nullable:false)
		primaryUser(blank:false)
	}
	
	static mapping = {
		taxa fetch: 'join'
		cache true;	
	}
}