package edu.uga.aligndb

class MajorClassification {

	String name
	
	static hasMany = [orders:TaxaOrder]
    static constraints = {
		name(blank:false)
    }
	public String toString(){
		return name
	}
	
	static mapping = {
		orders fetch: 'join'
	}
}
