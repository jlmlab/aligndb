package edu.uga.aligndb

class TaxaOrder {
	String name
	static belongsTo = [mc:MajorClassification]
	static hasMany = [families:Family]
    static constraints = {
		name(blank:false)
    }
	
	static mapping = {
		
		families fetch:'join'
		
	}
	
	public String toString(){
		return name
	}
}
