package edu.uga.aligndb
import org.grails.comments.*;
import java.util.Date;

class Sequence implements Commentable{
	
//	Taxa taxa;
//	Gene gene;

	
	String sequence
	String alignedSequence
	String state
	Boolean theOne = false
	Date dateCreated 
	Date lastUpdated
	User primaryUser
	
	static belongsTo = [taxa:Taxa, gene:Gene]
//    static transients = ['sequence','alignedSequence','state']	                  
	
    static constraints = {
		taxa()
		gene(nullable:true)
		state(inList:['Incomplete','Complete'])
		theOne()
		sequence(blank:false,nullable:false,maxSize:10000)
    	alignedSequence(nullable:true, blank:true,maxSize:10000)
    	dateCreated()
    	lastUpdated()
		primaryUser(blank:false)
	}

	static mapping = {
		taxa fetch: 'join'
		gene fetch: 'join'
		cache true;
	}

	
	String toString() {
		String tmpStr = "";
		tmpStr = taxa.toString() + " : " + gene.toString();
		return tmpStr;
	}
	
	String whatIsTheState(){
		if (state == 'Incomplete'){
			return 'I'
		}else{
			return 'C'
		}
	}
	
}
