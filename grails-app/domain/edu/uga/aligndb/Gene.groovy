package edu.uga.aligndb
import org.grails.comments.*;

class Gene implements Commentable{
	String name
	static hasMany = [sequences:Sequence]
	
	static constraints = {
		name(blank:false)
		sequences(cascade: "all-delete-orphan")
    }
	
	static mapping = {
		sort "name";
		cache true;
	}
	
	Integer totalSequenceCount() {
		return sequences.size()
	}
	
	String toString() {
		return name;
	}
}
