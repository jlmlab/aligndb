package edu.uga.aligndb

class Family {
	
	String name
	
	static hasMany = [taxa:Taxa]
	static belongsTo = [order:TaxaOrder]
    static constraints = {
		name(blank:false)
    }
	
	static mapping = {
		
		taxa fetch :'join'
		
	}
	
	public String toString(){
		return name
	}
}
