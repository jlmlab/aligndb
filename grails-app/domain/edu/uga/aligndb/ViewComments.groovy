package edu.uga.aligndb
import org.grails.comments.*;


class ViewComments implements Commentable{
	String controllerName;
	String viewName;
    static constraints = {
		controllerName(blank:false)
		viewName(blank:false)
    }
}
