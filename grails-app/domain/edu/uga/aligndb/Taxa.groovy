package edu.uga.aligndb

import org.grails.comments.*;

class Taxa implements Commentable{
	String name
	static hasMany = [sequences:Sequence, genome:Genome]
	static belongsTo = [family:Family]
    static constraints = {
    	name(blank:false)
    	sequences(cascade: "all-delete-orphan")
    }
	
	static mapping = {
		sort "name";
		cache true;
	}
	
	String toString() {
		return name;
	}
}