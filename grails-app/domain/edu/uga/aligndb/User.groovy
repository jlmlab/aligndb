package edu.uga.aligndb

class User {

	String username
	String password
	String firstName
	String lastName
	String email
	boolean enabled
	boolean accountExpired
	boolean accountLocked
	boolean passwordExpired


	static constraints = {
		username blank: false, unique: true
		password blank: false
		firstName blank: false
		lastName blank: false
		email blank: false
	}

	static mapping = {
		password column: '`password`'
	}

	Set<Role> getAuthorities() {
		UserRole.findAllByUser(this).collect { it.role } as Set
	}
}
